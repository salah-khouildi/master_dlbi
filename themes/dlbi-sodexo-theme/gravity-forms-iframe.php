<?php $is_included = false;
$width = 0;
if(isset($_REQUEST["width"])){
	$width = $_REQUEST["width"] * 1;
}
if(isset($_REQUEST["ajaxIncluded"])){
	$is_included = true;

	add_filter('gform_submit_button_' . $form_id, 'add_close_modal_button_after_button', 10, 2);
	add_filter('gform_next_button_' . $form_id, 'add_close_modal_button_after_button', 10, 2);
}
add_filter('gform_submit_button_' . $form_id, 'add_placeholder_before_button', 10, 2);
add_filter('gform_next_button_' . $form_id, 'add_placeholder_before_button', 10, 2);
add_filter('gform_previous_button_' . $form_id, 'add_placeholder_after_button', 10, 2);
if(!$is_included){
?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo esc_html($form['title']); ?></title>
		<?php } ?>
        <style type="text/css">
            <?php if(!$is_included){ ?>
            body {
                padding: 0;
                font-family: sans-serif;
                font-size: 13px;
            }

            <?php } ?>

            .gf-iframe {
                visibility: hidden;
            }

            .gf-iframe.fixed {
                visibility: visible;
            }

            .gf-iframe.ajax-loaded {
                position: relative;
            }

            .gf-iframe .image-wrapper {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                position: absolute;
            }

            .gf-iframe form {
                position: relative;
                margin-bottom: 30px !important;
            }

            .gf-iframe form .gform_heading {
                width: 90%;
                margin-left: auto;
                margin-right: auto;
            }

            .gf-iframe .ui-menu {
                list-style-type: none;
                background: white;
            }

            .gf-iframe .gform_wrapper .gform_footer {
                min-height: 75px;
                width: 90%;
                margin: 0 auto;

                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
            }

            .gf-iframe .gform_wrapper .gform_page_footer {
                min-height: 54px;
                width: 100%;
                margin: 10px 0 0 0 !important;

                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
            }

            .gf-iframe .gform_wrapper .gform_footer input[type=submit] {
                margin: 0;
            }

            .gf-iframe .gform_wrapper .gform_page_footer input[type=submit],
            .gf-iframe .gform_wrapper .gform_page_footer input.gform_next_button,
            .gf-iframe .gform_wrapper .gform_page_footer input.gform_previous_button {
                margin: 0;
            }

            .gf-iframe .gform_wrapper .gform_page_footer input.gform_previous_button {
                -webkit-box-ordinal-group: 2;
                -ms-flex-order: 1;
                order: 1
            }

            .gf-iframe .gform_wrapper .gform_page_footer input.gform_next_button {
                -webkit-box-ordinal-group: 11;
                -ms-flex-order: 10;
                order: 10
            }

            .gf-iframe .gform_page_footer input[type=submit] {
                -webkit-box-ordinal-group: 11;
                -ms-flex-order: 10;
                order: 10;

                margin: 0;
            }

            .gf-iframe .gform_footer input[type=submit] {
                margin-right: 0;

                -webkit-box-ordinal-group: 11;
                -ms-flex-order: 10;
                order: 10
            }

            .gf-iframe .placeholder {
                -webkit-box-ordinal-group: 4;
                -ms-flex-order: 3;
                order: 3
            }

            .gf-iframe .placeholder:only-of-type, .gf-iframe .placeholder ~ .placeholder {
                -webkit-box-ordinal-group: 7;
                -ms-flex-order: 6;
                order: 6
            }

            .gf-iframe .close-modal-button {
                margin: 0 !important;
                display: none;

                -webkit-box-ordinal-group: 6;
                -ms-flex-order: 5;
                order: 5
            }

            @media (max-width: 1023px) {
                .gf-iframe .close-modal-button {
                    display: inline-block;
                }

                .gf-iframe div input ~ input ~ .close-modal-button {
                    display: none;
                }
            }

            .gf-iframe .ui-helper-hidden-accessible {
                display: none !important;
            }

            .gf-iframe .di[src*="gf-img.png"] {
                background-color: #232B2C;
            }

            .gf-iframe form .gf_progressbar_wrapper {
                position: relative;
                top: 0;
                left: 5%;
                width: 90%;
                padding-bottom: 30px;
                /*display: none;*/
            }

            .gf-iframe form .gf_progressbar_wrapper.hidden {
                display: none;
            }

            .gf-iframe form .gf_progressbar_wrapper .gf_progressbar {
                position: static !important;
                margin-top: -18px;
            }

            .gf-iframe form .gf_progressbar_wrapper .gf_progressbar_percentage {
                margin-top: 5px;
            }

            .gf-iframe .gf_progressbar_title {
                position: absolute;
                top: 18px;
            }

            .gf-iframe .gf_progressbar_title:after {
                content: none;
            }

            .gf-iframe form .gform_title.no-underscore > div span:after {
                content: none;
                display: none;
            }

            .gf-iframe .gform_heading {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
            }

            .gf-iframe .gform_title {
                width: 100%;
                font-size: 23px !important;
            }

            .gf-iframe .only-title-wrapper + .gform_wrapper .gform_heading {
                margin-bottom: 45px;
            }

            .gf-iframe .confirmation-close-button-wrapper {
                display: none;
                width: 90%;
                margin: 10px auto;
                text-align: center;
            }

            .gf-iframe .gform_confirmation_wrapper + .confirmation-close-button-wrapper {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex
            }
        </style>
		<?php do_action('gfiframe_head', $form_id, $form);
		GFFormDisplay::print_form_scripts($form, false); // ajax = false ?

		$site_url           = trailingslashit(get_site_url());
		$display_background = true;

		$title         = false;
		$title_display = "";
		$classOverlay  = "";

		$img               = get_stylesheet_directory_uri() . '/dist/images/gf-img.png';
		$class             = "";
		$backgrounds       = get_field('gf_backgrounds', 'option');
		$color_title_bar   = "#23337f";
		$display_title_bar = false;
		$addon_class       = "";
		$image_width       = 0;
		$image_height      = 171; //default image height
		if($backgrounds){
			foreach($backgrounds as $background){
				if($background['gf_form'] == $form_id){
					$display_background = !$background["gf_background_image_not_displayed"];
					if(isset($background["gf_toolbar_not_displayed"])){
						$display_title_bar = !$background["gf_toolbar_not_displayed"];
					}
					if(isset($background["gf_title_bar_not_displayed"])){
						$display_title_bar = !$background["gf_title_bar_not_displayed"];
					}
					if($background['gf_background_image']['sizes']['soxo-hero-header']){
						$img = $background['gf_background_image']['sizes']['soxo-hero-header'];
					}
					$color_title_bar = $background["gf_progressbar_title_color"];
					break;
				}
			}
		}
		//	dlbi_display_debug($color_title_bar, 0, "blue");
		$size = dlbi_size_by_src($img);
		if($size){
			$image_width  = $size["width"];
			$image_height = $size["height"];
		}
		$image_top = "";
		if($display_background){
			$image_top = "<div class=\"image-wrapper $addon_class\">" . apply_filters("dlbi_image", $img, $class) . $title_display . "</div>";
		}else{
			$image_top = "   <div class=\"only-title-wrapper $addon_class\">$title_display</div>";
		}
		ob_start();
		gravity_form($form_id, $display_title, $display_description);
		$content = ob_get_contents();
		ob_end_clean();


		if($display_background){
			if(strpos($content, "gform_heading") !== false){
				$content = str_replace("class='gform_heading'", "class='gform_heading' style='height:$image_height" . "px'", $content);
				$content = str_replace("gform_confirmation_wrapper'", "gform_confirmation_wrapper' style='padding-top:$image_height" . "px'", $content);
			}else{
				$content = str_replace("class='gform_body'", "class='gform_body no-title' ", $content);
			}
		}

		if($color_title_bar !== "inherit"){
			//		dlbi_display_debug("hreere", 0, $color_title_bar);
			$content = str_replace("gf_progressbar_title'", "gf_progressbar_title' style='color:$color_title_bar !important'", $content);
		}
		//	$content = str_replace("class='gform_footer top_label'>", "class='gform_footer top_label'>", $content);

		//	if(strpos($content, "gf_browser_safari") !== false){
		//
		//	}

		if(!$display_title_bar){
			$content = str_replace("gform_title", "gform_title no-underscore", $content);
		}


		if(!$is_included){ ?>
    </head>
    <body class="gf-iframe">
		<?php }
		if($is_included){
		?>
        <div class="gf-iframe ajax-loaded">
			<?php }

			echo $image_top;
			echo $content;
			?>
            <div class="confirmation-close-button-wrapper">
				<?php echo dlbi_close_modal_button(); ?>
            </div>
			<?php


			if(!$is_included){
				wp_footer();
			} ?>
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
            <!--<script type="text/javascript" src="/app/themes/dlbi-sodexo-theme/dist/scripts/main.min.js?ver=0.3.0"></script>-->
			<?php if(is_plugin_active('dlbi-dqe/dlbi-dqe.php')){ ?>
                <script type='text/javascript' src='<?php echo $site_url ?>app/plugins/dlbi-dqe/assets/js/jquery.dqe.js'></script>
                <script type='text/javascript' src='<?php echo $site_url ?>app/plugins/dlbi-dqe/assets/js/jquery.dqeb2b.js'></script>
			<?php } ?>

            <script type="text/javascript">
                function replaceJbyFind(text) {
                    var ret = text;
                    ret = ret.replace("jQuery(", "jQuery(this).parents('.gf-iframe').find(");

                    var exploded = ret.split(";");
                    if (exploded[1]) {
                        ret = exploded[0] + ";" + "fakesubmit(jQuery(this).parents('form,[data-action]'));";
                    }
                    if (exploded[2]) {
                        ret += exploded[2];
                    }
                    return ret;
                }

                String.prototype.replaceAll = function (search, replacement) {
                    var target = this;
                    return target.replace(new RegExp(search, 'g'), replacement);
                };
                //remove step
                var str = jQuery('.gf_progressbar_title').text();
                var res = str.split("-");
                var text = '';
                for (var i = 1; i < 20; i++)
                    if (res[i]) {
                        text += ' ' + res[i];
                    }
                jQuery('.gf_progressbar_title').text(text);
                //hide cookie info bar
                jQuery("#cookie-notice").addClass('hide');
				<?php if (is_plugin_active('dlbi-dqe/dlbi-dqe.php')): ?>
                //Zip Code
                var DQEFields = ['.social-dqe', '.sirret-dqe', '.zipcode-dqe', '.city-dqe', '.street-dqe'];
                jQuery.each(DQEFields, function (key, selector) {
                    var item = jQuery(selector);
                    if (item.length) {
                        item.closest(".ginput_container").addClass("ui-front");
                        //the ui-font class is the root that receive autocomplete ui
                    }
                });


                //Social
                var interval = setInterval(function () {
                    console.log("interval");

                    try {
                        jQuery('#'<?php $form_id ?>).dqe({
                            server: '<?php echo $site_url ?>app/plugins/dlbi-dqe/assets/php/dqe.php',
                            // single : '.street-dqe',
                            zip: '.zipcode-dqe',
                            city: '.city-dqe',
                            street: '.street-dqe'
                        });

                        b2b = jQuery('#<?php $form_id ?>').dqeb2b({
                            server: '<?php echo $site_url ?>app/plugins/dlbi-dqe/assets/php/dqe.php',
                            ac_company: '.social-dqe',
                            ac_callback: 'callback_b2b_autocomplete'
                        });

                        clearInterval(interval);
                    } catch (e) {
                        console.log("try failed");
                    }


                }, 500);


                function callback_b2b_autocomplete(data) {
                    jQuery('.social-dqe').val(data.CompanyName);
                    jQuery('.sirret-dqe').val(data.CompanyNumber);
                    jQuery('.zipcode-dqe').val(data.ZIP_Code);
                    jQuery('.city-dqe').val(data.Locality);
                    jQuery('.street-dqe').val(data.CompanyAddress2);
                }


                // //Email
                // var dqemail = jQuery('.email-dqe').dqemail({
                // server: '<?php //echo $site_url ?>app/plugins/dlbi-dqe/assets/php/dqe.php',
                //         // last_name: '#my_lastname_field',
                //         // first_name: '#my_firstname_field'
                // });
                // //Phone
                // var dqephone = jQuery('.phone-dqe').dqephone({
                // server: '<?php //echo $site_url ?>app/plugins/dlbi-dqe/assets/php/dqe.php',
                //         // format: 0,
                //         // autocheck: false,
                //         // country: '#my_country_field'
                // });
				<?php endif ?>
				<?php if(dlbi_test_mode_active()){ ?>
                console.log("SOXINT-818", "<?php echo $width ?>");
				<?php } ?>
                var $gfIframe = jQuery(".gf-iframe");
                if ($gfIframe.length) {

                    if (jQuery(".ajax-loaded").length) {
                        jQuery(".gf-iframe input[type=submit]").attr("onclick", "");
                        var $buttonNextPrev = jQuery(".gf-iframe input[type=button]");
                        if ($buttonNextPrev.length) {
                            $buttonNextPrev.each(function () {
                                var onclick = jQuery(this).attr("onclick");
                                jQuery(this).attr("onclick", replaceJbyFind(onclick));
                                var onkeypress = jQuery(this).attr("onkeypress");
                                jQuery(this).attr("onkeypress", replaceJbyFind(onkeypress));
                            })
                        }
                    }

                    var $divtitle = jQuery(".gform_title").find("div");
                    if ($divtitle.length) {
                        $divtitle.html("<span>" + $divtitle.html() + "<span>");
                    }

                    jQuery(document).ready(function () {
                        // var fixed = checkCheckBox();
                        // console.log("readyHere", fixed);
                        console.log("readyHere");
                        fixGfIframeHeight();

                        setTimeout(fixGfIframeHeight, 500);
                        setTimeout(fixGfIframeHeight, 1000);
                        setTimeout(function () {
                            fixGfIframeHeight();
                            $gfIframe.addClass("fixed");

                        }, 2000);
                    });

                    jQuery(window).on("resize", function () {
                        fixGfIframeHeight();
                    });
                }


                function checkCheckBox() {
                    var fieldCheckbox = jQuery(".gfield_checkbox");
                    var found = false;
                    if (fieldCheckbox.length && !fieldCheckbox.hasClass('fixed')) {
                        fieldCheckbox.find("li").each(function () {
                            var input = jQuery(this).find("input[type='checkbox']");
                            var choiceImage = jQuery(this).find("label img");
                            console.log("checkCheckBox", input.length, choiceImage.length, !input.is(":hidden"));
                            if (input.length && choiceImage.length && !input.is(":hidden")) {

                                found = true;
                            }
                        });
                    }
                    if (found) {
                        fieldCheckbox.addClass("fixed");
                    }
                    return found;
                }

                function fixGfIframeHeight() {
                    // console.log("fixGfIframeHeight");
                    var $image = $gfIframe.find(".image-wrapper");
                    var $head = $gfIframe.find(".gform_heading");
                    var $form = $gfIframe.find("form");
                    var $confirmation = $gfIframe.find(".gform_confirmation_wrapper");
                    var $progressBar = $gfIframe.find(".gf_progressbar_wrapper");

                    var marginTop = 0;
                    var height = 0;
                    if ($image.length) {
                        height = $image.height();
                    }
                    // var $wrapper = $gfIframe.find(".gform_wrapper");
                    var $notitle = $gfIframe.find(".no-title");
                    if ($progressBar.length) {
                        $form.css("padding-top", "26px");
                        marginTop -= 26;
                    }

                    if ($image.length && $head.length) {
                        $head.css("height", height + "px");
                        if (marginTop) {
                            $head.css("margin-top", marginTop + "px");
                        }
                    }
                    if ($image.length && $confirmation.length) {
                        $confirmation.css("padding-top", height + "px");
                    }
                    if ($image.length && $notitle.length) {
                        $gfIframe.css("padding-top", height + "px");
                    }


                    //fix bug when loaded with default image soxint_824
                    //edited in SOXINT-796-P4
                    function checkHeight() {
                        var formFields = $gfIframe.find(".gform_fields");
                        var top1 = formFields.offset().top;
                        var top2 = $gfIframe.offset().top;
                        if (top1 === top2) {
                            console.log("checkHeight");
                            var imageDi = $gfIframe.find('.di');
                            var title = $gfIframe.find(".gform_title");
                            if (imageDi.length && !title.length) {
                                $gfIframe.css("padding-top", imageDi.height() + "px");
                            }
                        }
                    }

                    setTimeout(checkHeight, 500);
                    setTimeout(checkHeight, 1000);
                    setTimeout(checkHeight, 2000);
                    checkCheckBox();
                }


                // var $browserIfIsIphone = jQuery(".gf_browser_iphone");
                //
                // if ($browserIfIsIphone.length) {
                //     //     //we are on one of these buggy iPhone/iPad, who scrolls automatically to the input, even pn a non scrollable item
                //
                //
                //     // var height = $body.height();
                //     var height = $browserIfIsIphone.height();
                //     console.log(height);
                //
                //     jQuery('html').css("height", height);
                // }
            </script>
			<?php if(!$is_included){ ?>
    </body>
</html>
<?php }else{ ?>
    </div>
<?php } ?>
