function loadMore( e ) {


  var $this = jQuery(e.currentTarget),
      data = $this.data('load-more');

  data.action = 'blog_action';

  jQuery.post(
    data.url,
    data,
    function(resp){

      resp = resp.data;

      var $items = jQuery( resp.html );

      if( data.grid ){
        jQuery( data.zone ).append( $items );
        window.setTimeout(function(){
          jQuery(data.zone).masonry( 'appended', $items );
        }, 100);
      }
      else{
        jQuery( data.zone ).append( $items );
      }

      if( !resp.itemsLeft ){
        $this.remove();
      }
      else{
        $this.data('index', resp.index );
      }

  });

}