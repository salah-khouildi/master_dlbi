<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$with_newsletter = false;
require get_template_directory_child() . '/footer.php';
