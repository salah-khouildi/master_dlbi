<?php if(get_field('posts_grid_show_title_page')){

	$simplelink = 'Simple link';
	$typeform   = 'Typeform';
	$youtube    = 'Youtube';
	?>
    <section class="block-posts-grid">

        <div class="container">

			<?php if(get_field('posts_grid_block_title')){ ?>
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="sodexo-title"><?php echo get_field('posts_grid_block_title'); ?></h1>
                    </div>
                </div>
			<?php } ?>

            <div class="row align-items-stretch">
				<?php
				if(have_rows('posts_grid')){
					foreach(get_field('posts_grid') as $row){

						$type       	= $row['posts_grid_type'];
						$image      	= $row['posts_grid_image'];
						$image_link 	= $row['posts_grid_image_link'];
						$post_grid_link = $row['posts_grid_link'];
						$cta      = [];
						$addClass = "";

						if(isset($row['posts_grid_cta'])){
							$cta      = $row['posts_grid_cta'];
							$addClass = "hasCta";
						}

						if($type != 'Image' && is_object($row['posts_grid_link'])){
							$linked_post_id = $row['posts_grid_link']->ID;
						}
						if ($type === 'Image') { ?>
							<div class="col-12 col-sm-6 col-md-4 sameheight">
								<div class="block-posts-grid_content block-posts-grid_post bpg <?php echo $addClass ?>">
									<div class="block-posts-grid_content--item <?php if($image_link) echo 'block-has-image';else echo 'block-has-no-image' ?>" style="background-image: url('<?php echo $image['url']; ?>');"
                                        <?php if ($image_link && isset($image_link['url'])):
                                            if ($image_link['target'] && isset($image_link['target'])): ?>
                                                onclick="window.open('<?php echo $image_link['url'] ?>', '<?php echo $image_link['target']?>');"
                                            <?php else: ?>
                                                onclick="location.href='<?php echo $image_link['url']?>'"
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    >
										<?php
										if($cta && isset($cta["title"])){
											$title  = $cta["title"];
											$url    = $cta["url"];
											$target = $cta["target"];
											$attr   = "";
											if($target){
												$attr = "target='$target'";
											}
										?>
											<div class="callToAction-wrapper">
												<a class="btn-sodexo callToAction" <?php echo $attr ?> href="<?php echo $url; ?>"><?php echo $title; ?></a>
											</div>
										<?php
										}
										?>
									</div>
								</div>
                            </div>
						<?php
						}else{
							$cta      = [];
							$addClass = "";
							if(isset($row['posts_grid_cta'])){
								$cta      = $row['posts_grid_cta'];
								$addClass = "hasCta";
							}
							?>
                            <div class="col-12 col-sm-6 col-md-4 sameheight ">
                                <div class="block-posts-grid_content block-posts-grid_post bpg <?php echo $addClass ?>">
                                    <a href="<?php the_permalink($linked_post_id); ?>" class="block-posts-grid_content--item">
                                        <div class="block-posts-grid_content--item_image">
											<?php echo apply_filters("dlbi_image", $image['sizes']['soxo-medium']); ?>
                                        </div>
                                        <div class="block-posts-grid_content--item_text">
                                            <h3><?php echo get_the_title($linked_post_id) ?></h3>
											<?php if($row['posts_grid_description']){ ?>
                                                <p><?php echo $row['posts_grid_description']; ?></p>
											<?php } ?>
                                        </div>
                                    </a>
									<?php
									if($cta && isset($cta["title"])){
										$title  = $cta["title"];
										$url    = $cta["url"];
										$target = $cta["target"];
										$attr   = "";
										if($target){
											$attr = "target='$target'";
										}
										?>
                                        <div class="callToAction-wrapper">
                                            <a class="btn-sodexo callToAction" <?php echo $attr ?> href="<?php echo $url; ?>"><?php echo $title; ?></a>
                                        </div>
										<?php
									}
									?>
                                </div>
                            </div>
							<?php
						}
					}
					wp_reset_postdata();
				}
				?>
            </div>
        </div>
    </section>
<?php }
