#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-07-07 07:16+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#: inc/class-client.php:19 inc/class-client.php:20
msgid "Products"
msgstr ""

#: inc/class-client.php:21
msgid "Add new"
msgstr ""

#: inc/class-client.php:22
msgid "Add new Product"
msgstr ""

#: inc/class-client.php:23
msgid "Edit Product"
msgstr ""

#: inc/class-client.php:24
msgid "New Product"
msgstr ""

#: inc/class-client.php:25
msgid "Show Product"
msgstr ""

#: inc/class-client.php:26
msgid "Search Products"
msgstr ""

#: inc/class-client.php:27
msgid "No Products found"
msgstr ""

#: inc/class-client.php:28
msgid "No Products found in trash"
msgstr ""

#: inc/class-client.php:29
msgid "Parent Product"
msgstr ""

#. Name of the plugin
msgid "Sodexo Products"
msgstr ""

#. Description of the plugin
msgid "Plugin for manage products custom post type"
msgstr ""

#. Author of the plugin
msgid "Digitas LBI"
msgstr ""

#. Author URI of the plugin
msgid "https://www.digitaslbi.com/"
msgstr ""
