<?php

class SodexoProduct_Client {

    public function __construct() {
        add_action('init', array($this, 'sodexo_product_register_cpt'));
        add_action('init', array($this, 'sodexo_product_register_taxonomies'));
    }

    /**
     * Register the product post type
     *
     * @return boolean
     * @author Digitas LBI
     */
    public static function sodexo_product_register_cpt() {

        $slug_products = get_field('url_product', 'option') ? get_field('url_product', 'option') : SOD_PRO_PTYPE;


        register_post_type(SOD_PRO_PTYPE, array(
            'labels' => array(
                'name' => __('Products', 'dlbi-sodexo-products'),
                'singular_name' => __('Products', 'dlbi-sodexo-products'),
                'add_new' => __('Add new', 'dlbi-sodexo-products'),
                'add_new_item' => __('Add new Product', 'dlbi-sodexo-products'),
                'edit_item' => __('Edit Product', 'dlbi-sodexo-products'),
                'new_item' => __('New Product', 'dlbi-sodexo-products'),
                'view_item' => __('Show Product', 'dlbi-sodexo-products'),
                'search_items' => __('Search Products', 'dlbi-sodexo-products'),
                'not_found' => __('No Products found', 'dlbi-sodexo-products'),
                'not_found_in_trash' => __('No Products found in trash', 'dlbi-sodexo-products'),
                'parent_item_colon' => __('Parent Product', 'dlbi-sodexo-products'),
            ),
            'description' => '',
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'map_meta_cap' => true,
            'capability_type' => 'post',
            'public' => true,
            'hierarchical' => false,
            'rewrite' => array(
                'slug' => $slug_products,
                'with_front' => false,
                'pages' => true,
                'feeds' => true,
                'hierarchical' => true,
            ),
            'has_archive' => false,
            //'query_var' => $slug_products,
            'supports' => array(
                0 => 'title',
                1 => 'editor',
                2 => 'thumbnail',
                3 => 'excerpt',
                4 => 'page-attributes'
            ),
            'show_ui' => true,
            'menu_position' => 25,
            'menu_icon' => 'dashicons-cart',
            'can_export' => true,
            'show_in_nav_menus' => true,
            'show_in_menu' => true,
        ));

        flush_rewrite_rules();

        return true;
    }

    /**
     * Register the product taxonomies
     *
     * @return boolean
     * @author Digitas LBI
     */
    public static function sodexo_product_register_taxonomies() {
        // create a new taxonomy
        register_taxonomy(
                __('category-product', 'dlbi-sodexo-products'), SOD_PRO_PTYPE, array(
            'show_admin_column' => true,
            'label' => __('Category', 'dlbi-sodexo-products'),
            'show_admin_column' => true,
            'rewrite' => array(
                'slug' => __('category-product', 'dlbi-sodexo-products'),
                'hierarchical' => 'true'),
            'capabilities' => array(
                'assign_terms' => 'edit_guides',
                'edit_terms' => 'publish_guides'
            ),
            'hierarchical' => true,
            'publicly_queryable' => false,
                )
        );

        return true;
    }

}
