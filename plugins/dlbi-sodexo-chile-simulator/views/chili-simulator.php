<section class="w-100">
        <div class="">
            <?php
            if (array_key_exists('widget_title', $atts)) : ?>
                <p class="sodexo-pretitle"><?php echo $atts['widget_title']; ?></p>
            <?php endif; ?>

            <div id="root" style="height: 100%">
        </div>

</section>

