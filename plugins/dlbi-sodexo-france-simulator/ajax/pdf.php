<?php
// Load WordPress
$wp_load = $_SERVER['DOCUMENT_ROOT'] . '/wp/wp-load.php';
require_once( $wp_load );

$sendbymail = false;

if ( isset( $_GET ) && ( ! empty( $_GET ) ) ) {
	$getvalues = "?";

	$i = 0;
	foreach ( $_GET as $value => $key ) {
		if ( $i == 0 ) {
			$getvalues = $getvalues . $value . "=" . $key;
		} else {
			$getvalues = $getvalues . "&" . $value . "=" . $key;
		}
		$i ++;
	}
}
if ( isset( $_POST ) && ( ! empty( $_POST ) ) ) {
	$sendbymail = true;


	if ( filter_var( $_POST['send_pdf_mail'], FILTER_VALIDATE_EMAIL ) ) {
		$postemail = $_POST['send_pdf_mail'];
	} else {
		$postemail = "error";
	}

	//Subject
	if ( get_field( 'pdf_mail_subject', 'option' ) ) {
		$subject = get_field( 'pdf_mail_subject', 'option' );
	} else {
		$subject = "Your estimation Sodexo";
	}
	//Message on the mail
	if ( get_field( 'pdf_message', 'option' ) ) {
		$message = get_field( 'pdf_message', 'option' );
	} else {
		$message = "Please find your estimation attached !";
	}
	$headers = array( 'Content-Type: text/html; charset=UTF-8' );
	//From mail
	if ( get_field( 'pdf_email_from', 'option' ) ) {
		$headers[] = 'From: Sodexo <' . get_field( 'pdf_email_from', 'option' ) . '>';
	} else {
		$headers[] = 'From: Sodexo <estimation@sodexo.com>';
	}
	//CC mail
	if ( get_field( 'pdf_message', 'option' ) ) {
		$headers[] = 'Cc: Sodexo <' . get_field( 'pdf_message', 'option' ) . '>';
	}
	//else $headers[] = 'Cc: Sodexo <sodexo@sodexo.com>';

}


$las_results = array( 'status' => 'error' );
$lb_erreur   = false;

$url_pdf = 'simulator.pdf';

//echo $getvalues;
//echo SOD_FRE_URL. '/ajax/print/index.php'.$getvalues.'\' ' . $url_pdf . '';

if ( ! $lb_erreur ) {
	if ( ! file_exists( $url_pdf ) || ( filemtime( $url_pdf ) + 1 < time() ) ) {
		//define('HTACCESS_LOGIN', 'carrefour-festif');
		//define('HTACCESS_PASSWORD', '9c18a1805a');
		exec( 'phantomjs ' . get_stylesheet_directory() . '/src/assets/scripts/rasterize.js \'' . SOD_FRE_URL . '/ajax/print/index.php' . $getvalues . '\' ' . $url_pdf . '' );

	}

	if ( file_exists( $url_pdf ) ) {
		if ( ! $_POST && $sendbymail == false ) {
			header( 'Content-Description: File Transfer' );
			header( 'Content-Type: application/octet-stream' );
			header( 'Content-Disposition: attachment; filename="' . basename( $url_pdf ) . '"' );
			header( 'Expires: 0' );
			header( 'Cache-Control: must-revalidate' );
			header( 'Pragma: public' );
			header( 'Content-Transfer-Encoding: binary' );
			header( 'Content-Length: ' . filesize( $url_pdf ) );
		}

		ob_clean();
		flush();
		readfile( $url_pdf );

		if ( $_POST && $sendbymail == true ) {

			wp_mail( $postemail, $subject, $message, $headers, $attachments = array( $url_pdf ) );


		}
		unlink( $url_pdf );
		exit;
	} else {
		_e( 'Désolé, une erreur est survenue lors de la génération du PDF.', 'dlbi-sodexo-french-simulator' );
	}
} else {
	_e( 'Désolé, une erreur est survenue.', 'dlbi-sodexo-french-simulator' );
}


?>
