<?php

/*
  Plugin Name: Sodexo luxembourg simulator
  Description: Plugin for Luxembourg simulator
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-sodexo-luxembourg-simulator
 */

// require 'plugin-update-checker/plugin-update-checker.php';
// $MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
//     'https://wp-update.dev.soxodlbi.xyz/?action=get_metadata&slug=dlbi-sodexo-luxembourg-simulator', //Metadata URL.
//     __FILE__, //Full path to the main plugin file.
//     'dlbi-sodexo-luxembourg-simulator' //Plugin slug. Usually it's the same as the name of the directory.
// );

// Plugin Consts
define('SOD_LUX_VERSION', '1.0');
define('SOD_LUX_URL', plugins_url('', __FILE__));
define('SOD_LUX_DIR', dirname(__FILE__));

//Load class
require( SOD_LUX_DIR . '/inc/class-client.php' );
require( SOD_LUX_DIR . '/inc/class-admin.php' );
require( SOD_LUX_DIR . '/inc/widgets/class-luxembourg-simulator.php' );

//Init
function dlbi_sodexo_luxembourg_sim_init() {
    new SodexoLuxembourgSimulator_Client();
    new SodexoWidget_LuxembourgSimulator();
    if(is_admin()){
	new SodexoLuxembourgSimulator_Admin();
    }
    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-sodexo-luxembourg-simulator', false, dirname(plugin_basename(__FILE__)) . '/languages/');

}

add_action('plugins_loaded', 'dlbi_sodexo_luxembourg_sim_init', 11);
