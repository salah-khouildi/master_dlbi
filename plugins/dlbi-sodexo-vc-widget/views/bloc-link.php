<?php
//List of attributs
$titles = vc_param_group_parse_atts($atts['titles']);

?>

<section class=" alt choose-sodexo">
    <div class="col-12 choose-sodexo--two_blocks">
        <div class="choose-sodexo--container bg-grey info-blocks">

          <?php if (array_key_exists('titles', $atts)) : ?>

                  <?php foreach ($titles as $title): ?>
                      <div class="bloc-link_container">

                        <?php if (array_key_exists('bloc_link_icon', $title)): ?>
                          <div class="bloc-link_container--image">
                            <?php
                            $image = $title['bloc_link_icon'];
                            if (!empty($image)):
                              echo wp_get_attachment_image($image);
                            endif;
                            ?>
                          </div>
                        <?php endif ?>

                        <?php if (array_key_exists('bloc_link_title', $title)): ?>
                          <div class="bloc-link_container--title">
                            <h3 class="sodexo-title"><?php echo $title['bloc_link_title']; ?></h3>
                          </div>
                        <?php endif ?>

                        <?php if (array_key_exists('bloc_link_description', $title)): ?>
                          <div class="bloc-link_container--subtitle">
                            <p class="mb-0"><?php echo $title['bloc_link_description']; ?></p>
                          </div>
                        <?php endif ?>

                        <?php if (array_key_exists('bloc_link_contact_module', $title) && $title['bloc_link_contact_module']):
                          //checkbox to choose the contact module (to do later after the contact widget)

                          else :
                            if (array_key_exists('bloc_link', $title)): ?>
                            <div class="bloc-link_container--cta">
                              <?php $linkField = vc_build_link($title['bloc_link']); ?>
                              <a class="btn-sodexo btn-sodexo-red" href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if ($linkField['target']) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
                            </div>
                          <?php endif;
                        endif; ?>
                      </div>

                  <?php endforeach; ?>
 
          <?php endif; ?>

    </div>
  </div>

</section>
