<?php
// CALL BACK
if ( get_field( 'call_back_show_block', 'option' ) ) : ?>
	<li>

		<?php $nb_element = count( get_field( 'call_back_repeater', 'option' ) ); ?>

		<button type="button" class="contact-us icon-phone" aria-controls="lmt-contact" aria-expanded="false">
			<span><?php echo __( 'Contact us', 'lbi-sodexo-theme' ); ?></span>
		</button>

		<div id="lmt-contact" class="lmt-layer-contact" aria-hidden="true">

			<button type="button" class="lmt-close">
				<span><?php echo __( 'Close overlay', 'lbi-sodexo-theme' ); ?></span>
			</button>

			<?php
			if ( $nb_element > 1 ) :
				?>
			<form action="" method="POST">
				<div class="header">
					<p class="tt">
						<label for="contact-input"><?php echo __( 'Contact us', 'lbi-sodexo-theme' ); ?></label>
						<select class="custom-select">
							<?php
							// One builds the options of the select which display the div
							foreach ( get_field( 'call_back_repeater', 'option' ) as $row ) :
								if ( $row['call_back_select_a_value'] ) :
									?>
									<option value="<?php echo sanitize_title( $row['call_back_select_a_value'] ); ?>"><?php echo $row['call_back_select_a_value']; ?></option>
									<?php
								endif;
							endforeach;
							?>
						</select>
					</p>
				</div>
			</form>
			<?php endif; ?>

			<?php if ( get_field( 'call_back_repeater', 'option' ) ) : ?>
				<?php
				foreach ( get_field( 'call_back_repeater', 'option' ) as $key => $row ) :
					if ( $key === 0 && $nb_element > 1 ) {
						continue;
					}
					?>

				<div id="<?php echo sanitize_title( $row['call_back_select_a_value'] ); ?>" class="lmt-contact-info 
									<?php
									if ( $nb_element === 1 ) {
										echo 'active'; }
									?>
				">
					<article>
						<header>
						  <div class="lmt-contact-info-icon">
							<?php
							if ( $row['call_back_icon_1'] ) {
								$picture = $row['call_back_icon_1'];
								echo apply_filters( 'dlbi_image', $picture );
							} else {
								?>
								<span class="assistance-icon">
								  <svg viewBox="85.5 13.5 45.6 39.9">
									<path d="M128.3,27.2l-3.7,0l-0.7,0.1l-0.1-0.7c-1.3-7.6-7.9-13.1-15.6-13.1c-7.7,0-14.2,5.5-15.6,13.1l-0.1,0.7
									l-0.8-0.1l-3.6,0c-1.5,0-2.7,1.2-2.7,2.7v7c0,1.5,1.2,2.7,2.7,2.7l4.3,0l0,1.2c0,4.8,3.8,8.8,8.6,9.1l0.7,0v0.7
									c0,1.5,1.2,2.7,2.7,2.7h7c1.5,0,2.7-1.2,2.7-2.7v-3.5c0-1.5-1.2-2.7-2.7-2.7h-7c-1.5,0-2.7,1.2-2.7,2.7v0.8l-0.8-0.1
									c-3.7-0.4-6.5-3.4-6.5-7.2V29.3c0-7.6,6.2-13.8,13.8-13.8c7.6,0,13.8,6.2,13.8,13.8l0,7.6c0,1.5,1.2,2.7,2.7,2.7h3.5
									c1.5,0,2.7-1.2,2.7-2.7v-7C131,28.4,129.8,27.2,128.3,27.2z M92.4,37.7h-5v-8.5h5V37.7z M103.7,46.5l0.8,0h7.8v5h-8.5V46.5z
									M129.1,37.7h-5v-8.5h5V37.7z M110.1,37c-0.4,0.6-1,1-1.7,1.1c-0.8,0.1-1.6-0.3-2.1-1c-0.3-0.5-1-0.7-1.5-0.3
									c-0.5,0.3-0.7,1-0.3,1.5c0.8,1.3,2.2,2.1,3.7,2.1c0.1,0,0.3,0,0.4,0c1.4-0.1,2.7-0.9,3.5-2.2c0.3-0.5,0.1-1.2-0.4-1.5
									C111.1,36.3,110.4,36.4,110.1,37z"/>
								  </svg>
								</span>
								<?php } ?>
							</div>

							<?php if ( $row['call_back_title'] ) : ?>
								<h3 class="tt"><?php echo $row['call_back_title']; ?></h3>
							<?php endif; ?>
						</header>

						<?php if ( $row['call_back_text'] ) : ?>
						<p><?php echo $row['call_back_text']; ?></p>
						<?php endif; ?>

						<?php if ( $row['call_back_telephone'] ) : ?>
						<a target="_blank" href="tel:<?php echo str_replace( [ ' ', '-', '_' ], '', $row['call_back_telephone'] ); ?>"><?php echo $row['call_back_telephone']; ?></a>
						<?php endif; ?>

						<?php if ( $row['call_back_link'] ) : ?>
						<p class="lmt-contact-email">

							<?php
							if ( $row['call_back_icon_2'] ) {

								$picture = $row['call_back_icon_2'];
								?>

							<a href="mailto:<?php echo $row['call_back_link']; ?>">
								  <?php echo apply_filters( 'dlbi_image', $picture ); ?><br>
								  <?php
									if ( $row['call_back_link_label'] ) {
										echo $row['call_back_link_label'];
									} else {
										echo __( 'Send us an email', 'lbi-sodexo-theme' );
									}
									?>
							</a>

								<?php } else { ?>

							<a href="mailto:<?php echo $row['call_back_link']; ?>">
							  <i class="icon-envelop"></i>
							  <br>
								<?php
								if ( $row['call_back_link_label'] ) {
									echo $row['call_back_link_label'];
								} else {
									echo __( 'Send us an email', 'lbi-sodexo-theme' );
								}
								?>
							</a>

							<?php } ?>

						</p>
						<?php endif; ?>
					</article>
				</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</li>
<?php endif ?>
