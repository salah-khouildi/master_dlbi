<?php
$titles = vc_param_group_parse_atts($atts['titles']);
?>
<?php if (array_key_exists('top-title', $atts)) : ?>
  <h2><?php echo $atts['top-title']; ?></h2>
<?php endif ?>
<div class="sodexo-widget-boxes">
  <?php if ($titles): ?>
    <div class="sodexo-widget-boxes-container">

      <?php foreach ($titles as $title) { ?>

        <div class="box">
          <div class="box-image">
            <?php
            if (array_key_exists('img_size', $title)) :
              echo wp_get_attachment_image($title['img_size']);
            endif;
            ?>
          </div>
          <?php if (array_key_exists('title', $title)): ?>
            <h3 class="box-title"><?php echo $title['title']; ?></h3>
          <?php endif ?>
          <?php if (array_key_exists('title', $title)): ?>
            <p class="box-text"><?php echo $title['text']; ?></p>
          <?php endif ?>
          <div class="box-line"></div>
        </div>

      <?php } ?>

    </div>
  <?php endif ?>

</div>
