<?php if (get_field('image_list_show')) { ?>

  <section class="block-image-list">

    <div class="container">
      <div class="container-padded">

        <div class="block-image-list_content">
          <?php if (get_field('image_list_title')): ?>
            <div class="row">
              <div class="col-lg-10 offset-lg-1">
                <h2 class="sodexo-title sodexo-title--white"><?php echo get_field('image_list_title'); ?></h2>
              </div>
            </div>
          <?php endif; ?>

          <div class="row">
            <?php if (get_field('image_list_image')): ?>
              <div class="col-md-6 align-self-center">
                <div class="block-image-list_content--image">
	                <?php echo apply_filters( "dlbi_image",get_field('image_list_image')['sizes']['large'],"img-fluid" ); ?>
                </div>
              </div>
            <?php endif; ?>

            <?php if (get_field('image_list_list')): ?>
              <div class="col-md-6">
                <ul class="block-image-list_content--list">
                  <?php foreach (get_field('image_list_list') as $item): ?>
                    <li>
                      <h3 class="soxo-check-icon"><?php echo $item['image_list_list_title'] ?></h3>
                      <?php echo $item['image_list_list_text'] ?>
                    </li>
                  <?php endforeach; ?>
                </ul>
              </div>
            <?php endif; ?>
          </div>

        </div>

      </div>
    </div>

  </section>

<?php } ?>
