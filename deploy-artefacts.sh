#!/bin/sh
set -eu
#set -x

print_usage() {
	cat << EOT
Usage
    ${0##*/} [-p path]
    Deploy plugins and themes onto the WP update server.
    E.g.: ${0##*/} -h dev.sodexo.digitas.fr -p /srv/sodexo/common/wp-update-server/packages/

#    -h scp host
#        e.g. "dev.sodexo.digitas.fr"

    -p path
        e.g. "/srv/sodexo/common/wp-update-server/packages/"
EOT
}


# see image_builder/compose-sodexo/wordpress/bin/gulp-build.sh
do_gulp() {
	for d in "$@"; do
		if [ -s "$d/gulpfile.js" ]; then
			cd "$d"
			if [ ! -d node_modules ]; then
				npm link gulp-cli
				npm install
			fi
			gulp build
			# CACHEDIR.TAG is for tar exclusion (via --exclude-caches-all): http://www.brynosaurus.com/cachedir/
			echo "Signature: 8a477f597d28d172789f06886806bc55" > node_modules/CACHEDIR.TAG
			cd -
		fi
	done
}


package_and_upload() {
	BASE_DIR="$1"
	cd "${BASE_DIR}"
	# cleanup old cached artefacts
	rm -f dlbi-*.zip
	# package and upload current version
	for f in  dlbi-*; do
		echo "	$f"
		zip -q9royT "$f.zip" "$f" -x "$f/node_modules/"
	done
	#scp -p -o StrictHostKeyChecking=no dlbi-*.zip "docker_usr@${SCP_HOST}:${SCP_PATH}"
	cp -a dlbi-*.zip "${SCP_PATH}"
	# package and upload archive version
	for f in  dlbi-*.zip; do
		local ARTEFACT_VERSION=
		if [ -f "${f%.zip}/style.css" ]; then
			# this is a theme: get version from style.css
			ARTEFACT_VERSION=$(grep -i "Version:" "${f%.zip}/style.css" | grep -o "[0-9][0-9a-zA-Z.]*")
		else
			# this is a plugin: get version from only php file in root directory
			ARTEFACT_VERSION=$(grep -i "Version:" "${f%.zip}"/*.php | grep -o "[0-9][0-9a-zA-Z.]*")
		fi
		if [ -z "$ARTEFACT_VERSION" ]; then
			echo "Error: can not compute artefact version for: ${f%.zip}"
			exit 1
		fi
		mv "$f" "${f%.zip}-$ARTEFACT_VERSION.zip"
	done
	#scp -p -o StrictHostKeyChecking=no dlbi-*.zip "docker_usr@${SCP_HOST}:${ARCHIVE_PATH}"
	cp -a dlbi-*.zip "${ARCHIVE_PATH}"
	rm -f dlbi-*.zip
	cd -
}


main() {
	#SCP_HOST=${SCP_HOST:-}
	SCP_PATH=${SCP_PATH:-}
	
	# Options
	while getopts "p:" option; do
		case "$option" in
			#h) SCP_HOST=$OPTARG ;;
			p) SCP_PATH=$OPTARG ;;
			*) print_usage; exit 1 ;;
		esac
	done
	shift $((OPTIND - 1))  # Shift off the options and optional --
	
	ARCHIVE_PATH=${ARCHIVE_PATH:-${SCP_PATH}/archive/}

	if [ -z "${SCP_PATH}" ]; then
		print_usage
		exit 1
	fi
	
	echo "Compiling static resources..."
	do_gulp themes/dlbi-*
	echo "Packaging plugins..."
	package_and_upload plugins
	echo "Packaging themes..."
	package_and_upload themes
}
main "$@"

