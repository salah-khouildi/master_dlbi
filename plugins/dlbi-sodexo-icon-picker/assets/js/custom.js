var nbLoops = 0;
var listIcons = [];
var listIconsSearch = [];
var folders = [];
jQuery(document).ready(function($) {
	var cssMenuInputs = $('div[data-name="custom-icon"] input').fontIconPicker();

	function handleResponse(response) {
		var classPrefix = response.preferences.fontPref.prefix,
				icomoon_json_icons = [],
				icomoon_json_search = [];

		// For each icon
		$.each(response.icons, function(i, v) {

				// Set the source
				icomoon_json_icons.push( classPrefix + v.properties.name );

				// Create and set the search source
				if ( v.icon && v.icon.tags && v.icon.tags.length ) {
						icomoon_json_search.push( v.properties.name + ' ' + v.icon.tags.join(' ') );
				} else {
						icomoon_json_search.push( v.properties.name );
				}
		});
		nbLoops++;

		listIcons = listIcons.concat(icomoon_json_icons);
		listIconsSearch = listIconsSearch.concat(icomoon_json_search);

		// Set new fonts on fontIconPicker if last loop
		if(nbLoops == folders.length+1) {
			cssMenuInputs.setIcons(listIcons, listIconsSearch);
		}
	}

	if(cssMenuInputs.length > 0 && starter_global.list_folders) {
		//Load default font
		$.ajax({
				url: starter_global.folder_font_default+'/selection.json',
				type: 'GET',
				dataType: 'json'
		})
		.done(handleResponse);

		//Load custom font
		folders = JSON.parse(starter_global.list_folders);
		for (var i = 0; i < folders.length; i++) {
			$.ajax({
		      url: starter_global.folder_font+'/'+folders[i]+'/selection.json',
		      type: 'GET',
		      dataType: 'json'
		  })
		  .done(handleResponse);
		}
	}
});
