<?php
/**
* The CTA displayed on Desktop
*
* @package understrap
* @subpackage dlbi-sodexo-theme
*/

$default    = get_field( 'lead_toolbar_custom_blocks', 'option' );
$lmt_layout = get_field( 'lead_managment_toolbar', get_the_ID() );

if ( $lmt_layout ) { // If custom LMT.
	$lmt_layout = $lmt_layout[0];
	$show_wcb   = get_field( 'lmt_show_web_call_back', get_the_ID() );
	$wcb_id     = get_field( 'lmt_web_call_back_id', get_the_ID() );
} else { // Else, general LMT.
	$show_wcb = get_field( 'web_callback_show', 'option' );
	$wcb_id   = get_field( 'web_callback_id', 'option' );
}

$custom = get_field( 'lead_toolbar_custom_blocks', $lmt_layout );
$blocks = $custom ? $custom : $default;
// template to display background image if any uploaded in BO.
$s_background_template = 'style=background-image:url(\'%s\');background-size:contain;background-repeat:no-repeat;background-position:left';

if ( $blocks ) :
	foreach ( $blocks as $key => $block ) :
		$show_block = $block['show_block'];
		$block_type = $block['type_of_block'][0]['acf_fc_layout'];

		if ( $show_block && 'external_link' === $block_type ) : ?>
		<div class="lmt-panel">

			<?php
			// LINK.
			$ext_link = $block['type_of_block'][0]['external_link'];
			if ( $ext_link ) :
				$custom_picto_ext_link = $block['type_of_block'][0]['external_link_picto'];
				$is_quotation          = $block['type_of_block'][0]['external_link']['title'];
				$s_style_background    = $s_class_icon = '';
				if ( $custom_picto_ext_link ) :
					$s_style_background = sprintf(
						$s_background_template,
						$custom_picto_ext_link['sizes']['medium']
					);
					else :
						$s_class_icon = 'icon-shopping';
					endif;
					?>
					<a onclick="dataLayer.push({'event': 'click-LMT', 'value': 'external-link'});" class="lmt-link" href="<?php echo esc_attr( $ext_link['url'] ); ?>" target="<?php echo esc_attr( $ext_link['target'] ); ?>">
						<button class="lmt-btn ext-link <?php if($key==0):echo 'soxo-red ';endif; echo esc_attr( $s_class_icon ); ?>" <?php echo esc_html( $s_style_background ); ?> >
							<span class="lmt-button-title"><?php echo esc_attr( $ext_link['title'] ); ?></span>
						</button>
					</a>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php
		if ( $show_block && 'search' === $block_type ) :
			$title              = $block['type_of_block'][0]['search_title'];
			$custom_picto       = isset( $block['type_of_block'][0]['search_picto'] ) && $block['type_of_block'][0]['search_picto'];
			$s_style_background = $s_class_icon = '';
			if ( $custom_picto ) :
				$s_style_background = sprintf(
					$s_background_template,
					$custom_picto['sizes']['medium']
				);
				else :
					$s_class_icon = 'icon-search';
				endif;

				$query_var = '';
				if ( ! empty( get_search_query() ) ) {
					$query_var = get_search_query();
				} elseif ( ! empty( $_GET['s'] ) ) {
					$query_var = esc_attr( $_GET['s'] );
				}
				?>
				<div class="lmt-panel">
					<button onclick="dataLayer.push({'event': 'click-LMT', 'value': 'search'});" type="button" class="lmt-btn <?php if($key==0):echo 'soxo-red ';endif; echo esc_attr( $s_class_icon ); ?>" data-toggle="collapse" data-target="#collapseSearch" aria-controls="collapseSearch" aria-expanded="false">
						<span class="lmt-button-title"><?php echo esc_attr_e( 'Search', 'lbi-sodexo-theme' ); ?></span>
					</button>
					<div class="collapse" id="collapseSearch">
						<div class="lmt-layer lmt-layer-search">
							<button type="button" class="lmt-close" data-toggle="collapse" data-target="#collapseSearch" aria-controls="collapseSearch" aria-expanded="false">
								<span class="sr-only"><?php echo esc_attr_e( 'Close overlay', 'lbi-sodexo-theme' ); ?></span>
							</button>
							<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="GET">
								<div class="header">
									<label for="s" class="tt"><?php esc_attr_e( 'Search', 'lbi-sodexo-theme' ); ?></label>
								</div>
								<div class="lmt-layer-search_block">
									<input type="search" name="s" id="s" value="<?php echo esc_attr( $query_var ); ?>" placeholder="<?php echo esc_attr( $title ); ?>">
									<button type="submit" class="submit-search icon-search">
										<span class="sr-only"><?php esc_attr_e( 'Search', 'lbi-sodexo-theme' ); ?></span>
									</button>
								</div>
								<input type="hidden" name="origin" value="LMT">
							</form>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<?php if ( $show_block && 'call_back' === $block_type ) : ?>

				<?php
				$elements   = $block['type_of_block'][0]['call_back_repeater'];
				$nb_element = count( $elements );
				?>
				<div class="lmt-panel">
					<?php
					$custom_picto_call_back = isset( $block['type_of_block'][0]['call_back_picto'] ) && $block['type_of_block'][0]['call_back_picto'];
					$s_style_background     = $s_class_icon = '';
					if ( $custom_picto_call_back ) :
						$s_style_background = sprintf(
							$s_background_template,
							$custom_picto_call_back['sizes']['medium']
						);
						else :
							$s_class_icon = 'icon-phone';
						endif;
						?>
						<button onclick="dataLayer.push({'event': 'click-LMT', 'value': 'contact-us'});" type="button" class="lmt-btn contact-us <?php if($key==0):echo 'soxo-red ';endif; echo esc_attr( $s_class_icon ); ?>" data-toggle="collapse" data-target="#collapseContact" aria-controls="collapseContact" aria-expanded="false" <?php echo $s_style_background; ?>>
							<span class="lmt-button-title"><?php echo esc_attr_e( 'Contact us', 'lbi-sodexo-theme' ); ?></span>
						</button>

						<div class="collapse" id="collapseContact">
							<div class="lmt-layer lmt-layer-contact">
								<button type="button" class="lmt-close" data-toggle="collapse" data-target="#collapseContact" aria-controls="collapseContact" aria-expanded="false">
									<span class="sr-only"><?php echo esc_attr_e( 'Close overlay', 'lbi-sodexo-theme' ); ?></span>
								</button>

								<?php if ( $nb_element > 1 ) : ?>
									<form action="" method="POST">
										<div class="header">
											<p class="tt">
												<label for="contact-input"><?php echo esc_attr_e( 'Contact us', 'lbi-sodexo-theme' ); ?></label>
												<select class="custom-select">
													<?php
													// One builds the options of the select which display the div.
													foreach ( $elements as $row ) :
														$select_value = $row['call_back_select_a_value'];
														if ( $select_value ) :
															?>
															<option value="<?php echo rawurldecode( sanitize_title( $select_value ) ); ?>"><?php echo esc_attr( $select_value ); ?></option>
															<?php
														endif;
													endforeach;
													?>
												</select>
											</p>
										</div>
									</form>
								<?php endif; ?>

								<?php
								if ( $elements ) :
									foreach ( $elements as $key => $row ) :
										$select_value = $row['call_back_select_a_value'];
										if ( 0 === $key && $nb_element > 1 ) {
											continue;
										}
										?>

										<div id="<?php echo rawurldecode( sanitize_title( $select_value ) ); ?>" class="lmt-contact-info
											<?php
											if ( 1 === $nb_element ) {
												echo 'active';
											}
											?>
											">
											<article>
												<header>
													<div class="lmt-contact-info-icon">
														<?php
														$picture = $row['call_back_icon_1'];
														if ( $picture ) {
															echo apply_filters( 'dlbi_image', $picture );
														} else {
															?>
															<span class="assistance-icon">
																<svg viewBox="85.5 13.5 45.6 39.9">
																	<path d="M128.3,27.2l-3.7,0l-0.7,0.1l-0.1-0.7c-1.3-7.6-7.9-13.1-15.6-13.1c-7.7,0-14.2,5.5-15.6,13.1l-0.1,0.7
																	l-0.8-0.1l-3.6,0c-1.5,0-2.7,1.2-2.7,2.7v7c0,1.5,1.2,2.7,2.7,2.7l4.3,0l0,1.2c0,4.8,3.8,8.8,8.6,9.1l0.7,0v0.7
																	c0,1.5,1.2,2.7,2.7,2.7h7c1.5,0,2.7-1.2,2.7-2.7v-3.5c0-1.5-1.2-2.7-2.7-2.7h-7c-1.5,0-2.7,1.2-2.7,2.7v0.8l-0.8-0.1
																	c-3.7-0.4-6.5-3.4-6.5-7.2V29.3c0-7.6,6.2-13.8,13.8-13.8c7.6,0,13.8,6.2,13.8,13.8l0,7.6c0,1.5,1.2,2.7,2.7,2.7h3.5
																	c1.5,0,2.7-1.2,2.7-2.7v-7C131,28.4,129.8,27.2,128.3,27.2z M92.4,37.7h-5v-8.5h5V37.7z M103.7,46.5l0.8,0h7.8v5h-8.5V46.5z
																	M129.1,37.7h-5v-8.5h5V37.7z M110.1,37c-0.4,0.6-1,1-1.7,1.1c-0.8,0.1-1.6-0.3-2.1-1c-0.3-0.5-1-0.7-1.5-0.3
																	c-0.5,0.3-0.7,1-0.3,1.5c0.8,1.3,2.2,2.1,3.7,2.1c0.1,0,0.3,0,0.4,0c1.4-0.1,2.7-0.9,3.5-2.2c0.3-0.5,0.1-1.2-0.4-1.5
																	C111.1,36.3,110.4,36.4,110.1,37z"/>
																</svg>
															</span>
														<?php } ?>
													</div>

													<?php
													$title = $row['call_back_title'];
													if ( $title ) :
														?>
														<h3 class="tt"><?php  $title ; ?></h3>
													<?php endif; ?>
												</header>

												<?php
												$text = $row['call_back_text'];
												if ( $text ) :
													?>
													<p><?php echo $text; ?></p>
													<?php
												endif;

												$phone = $row['call_back_telephone'];
												if ( $phone ) :
													?>
													<a target="_blank" href="tel:<?php echo str_replace( [ ' ', '-', '_' ], '', $phone ); ?>"><?php echo esc_attr( $phone ); ?></a>
													<?php
												endif;

												$link  = apply_filters( 'dlbi_filter_https', $row['call_back_link'] );
												$label = $row['call_back_link_label'];
												if ( $link ) :
													?>
													<p class="lmt-contact-email">

														<?php
														$picture = $row['call_back_icon_2'];
														if ( $picture ) {
															?>

															<a href="mailto:<?php echo $link; ?>">
																<?php echo apply_filters( 'dlbi_image', $picture ); ?><br>
																<?php
																if ( $label ) {
																	echo esc_attr( $label );
																} else {
																	echo esc_attr_e( 'Send us an email', 'lbi-sodexo-theme' );
																}
																?>
															</a>

														<?php } else { ?>

															<a href="mailto:<?php echo esc_attr( $link ); ?>">
																<i class="icon-envelop"></i>
																<br>
																<?php
																if ( $label ) {
																	echo esc_attr( $label );
																} else {
																	echo esc_attr_e( 'Send us an email', 'lbi-sodexo-theme' );
																}
																?>
															</a>

														<?php } ?>

													</p>
												<?php endif; ?>

												<?php if ( $show_wcb && $wcb_id ) : ?>
													<div class="lmt-webcallback">
														<a class="btn-sodexo btn-sodexo-red LnkWcbForm-trigger"><?php echo get_field( 'web_callback_cta_label', 'option' ); ?></a>
													</div>
												<?php endif ?>

											</article>
										</div>
										<?php
									endforeach;
								endif;
								?>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<?php if ( $show_block && 'request_a_quote' === $block_type ) : ?>

					<div class="lmt-panel">

						<?php
						// LINK.
						$link = apply_filters( 'dlbi_filter_https', $block['type_of_block'][0]['request_a_quote_link'] );
						if ( $link ) :
							$title                      = $block['type_of_block'][0]['request_a_quote_title'];
							$target = $block['type_of_block'][0]['request_a_quote_link_tab'] ? '_blank' : '_self';
							$custom_picto_request_quote = isset( $block['type_of_block'][0]['request_a_quote_picto'] ) && $block['type_of_block'][0]['request_a_quote_picto'];
							$s_style_background         = $s_class_icon = '';
							if ( $custom_picto_request_quote ) :
								$s_style_background = sprintf(
									$s_background_template,
									$custom_picto_request_quote['sizes']['medium']
								);
								else :
									$s_class_icon = 'icon-messages';
								endif;
								?>
								<?php if($block['type_of_block'][0]['request_a_quote_link_type'] != 'modal'): ?>
									<button onclick="window.open('<?php echo esc_attr( $link ); ?>','<?php echo $target ?>');" type="button" class="lmt-btn quote <?php if($key==0):echo 'soxo-red ';endif; echo esc_attr( $s_class_icon ); ?>" <?php echo esc_attr( $s_style_background ); ?>>
										<span class="lmt-button-title"><?php echo esc_attr( $title ); ?></span>
									</button>
								<?php else : ?>
									<button data-toggle="modal" data-target="#typeformModal"  data-typeform="<?php echo esc_attr( $link ); ?>" type="button" class="lmt-btn quote <?php if($key==0):echo 'soxo-red ';endif; echo esc_attr( $s_class_icon ); ?>" aria-expanded="false" <?php echo esc_attr( $s_style_background ); ?>>
										<span class="lmt-button-title"><?php echo esc_attr( $title ); ?></span>
									</button>
								<?php endif ?>
							<?php endif; ?>

						</div>
					<?php endif; ?>

					<?php if ( $show_block && 'store_locator' === $block_type ) : ?>

						<div class="lmt-panel">

							<?php
							$link_store_loca_title      = $block['type_of_block'][0]['store_locator_lmt_title'];
							$link_store_loca            = $block['type_of_block'][0]['links'];
							$custom_picto_store_locator = $block['type_of_block'][0]['store_locator_picto'];
							?>
							<?php if ( $link_store_loca[0]['link'] ) : ?>
								<a onclick="dataLayer.push({'event': 'click-LMT', 'value': 'store-locator'});" class="lmt-link" href="<?php echo esc_attr( $link_store_loca[0]['link']['url'] ); ?>" target="_blank">
									<button class="lmt-btn ext-link <?php if($key==0):echo ' soxo-red';endif; if ( ! $custom_picto_store_locator ) { echo ' icon-locator'; } ?>"
										<?php if ( $custom_picto_store_locator ) : ?>
											style="background-image:url('<?php echo esc_attr( $custom_picto_store_locator['sizes']['medium'] ); ?>');background-size:contain;background-repeat:no-repeat;background-position:left"<?php endif ?>>
											<span class="lmt-button-title"><?php echo esc_attr( $link_store_loca_title ); ?></span>
										</button>
									</a>
								<?php else : ?>
									<button onclick="dataLayer.push({'event': 'click-LMT', 'value': 'store-locator'});" type="button"
									<?php
									if($key==0):echo 'class="lmt-btn locator soxo-red' . $icon_locator = ( ! $custom_picto_store_locator ) ? ' icon-locator"' : '" data-toggle="collapse" data-target="#collapseStoreloca" aria-controls="collapseStoreloca" aria-expanded="false" ';
								else:echo 'class="lmt-btn locator' . $icon_locator = ( ! $custom_picto_store_locator ) ? ' icon-locator"' : '" data-toggle="collapse" data-target="#collapseStoreloca" aria-controls="collapseStoreloca" aria-expanded="false" ';
							endif;
							if ( $custom_picto_store_locator ) :
								?>
								style="background-image:url('<?php echo esc_attr( $custom_picto_store_locator['sizes']['medium'] ); ?>');background-size:contain;background-repeat:no-repeat;background-position:left"
							<?php endif ?>>
							<span class="lmt-button-title"><?php echo esc_attr( $link_store_loca_title ); ?></span>
						</button>
					<?php endif ?>
					<?php if ( $link_store_loca && count( $link_store_loca ) > 1 ) : ?>

						<div class="collapse" id="collapseStoreloca">
							<div class="lmt-layer lmt-layer-storeloca">
								<button type="button" class="lmt-close" data-toggle="collapse" data-target="#collapseStoreloca" aria-controls="collapseStoreloca" aria-expanded="false">
									<span class="sr-only"><?php echo esc_attr_e( 'Close overlay', 'lbi-sodexo-theme' ); ?></span>
								</button>
								<div class="header">
									<p class="tt"><?php echo esc_attr( $link_store_loca_title ); ?></p>
								</div>
								<div class="lmt-layer-storeloca_block">
									<?php foreach ( $link_store_loca as $link ) : ?>
										<a class="btn-sodexo btn-sodexo-red" href="<?php echo esc_attr( $link['link']['url'] ); ?>" target="<?php echo esc_attr( $link['link']['target'] ); ?>">
											<?php echo esc_attr( $link['link']['title'] ); ?>
										</a>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
