/**
 * Close login dropdown menus when scrolling viewport
 */
var sxode_menuLogin = jQuery('.loginDropdown');

jQuery(window).on('scroll', false, function () {
    var scrollY = jQuery(document).scrollTop();

    if (scrollY < 600 || scrollY > 700) {
        return false;
    }

    if (sxode_menuLogin.hasClass('show')) {
        sxode_menuLogin.removeClass('show');
        jQuery('.loginDropdown__link').attr('aria-expanded', 'false').blur();
        jQuery('.loginDropdown__menu').removeClass('show');
    }
});

/**
 * Remove focus when closing login dropdown
 */
sxode_menuLogin.on('click', function () {
    if (sxode_menuLogin.hasClass('show')) {
        jQuery('.loginDropdown__link').blur();
    }
});