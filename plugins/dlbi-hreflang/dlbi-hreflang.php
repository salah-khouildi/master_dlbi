<?php

/*
Plugin Name: DLBI-hreflang
Description: Plugin witch allow user to add infinite <link rel="alternate" hreflang="x"> for each page
Version: 1.0.1
Author: Thibault@Digitas LBI
Author URI: https://www.digitaslbi.com/
License: GPLv2 or later
*/

class DlbiHreflang{
	static $root = __DIR__;
	static $file = __FILE__;
	static $name = "dlbi-hreflang";

	public static function instance(){
		static $inst = null;
		if($inst === null){
			$inst = new self();
		}

		return $inst;
	}


	private function __construct(){
		add_action('admin_enqueue_scripts', [$this, 'enqueue_assets'], 100);
		add_action('add_meta_box', [$this, 'add_metabox'], 100);
		//		add_action('admin_enqueue_scripts', [$this, 'enqueue_assets'], 100);
		add_action('wp_head', [$this, 'enqueue_assets'], 100);
		add_action('wp_head', [$this, 'add_lang'], 100);
		add_action('wp_ajax_get_current_href_lang', [$this, 'get_href_lang_from_page'], 100);
		//		add_filter('the_content', [$this, 'debug'], 1);

	}

	/**
	 * This function have been made to allow theme to override admin's js path of the plugin
	 * @return string
	 * Get the admin js file path of the plugin : By default it is located in {current path}/script/dlbi-hreflang.js
	 * can be overridden in 2 ways
	 * - by adding a file in {current theme path}/plugins/dlbi-hreflang/script/dlbi-hreflang.js (1)
	 * - by using the dlbi-hreflang-script-url filter witch override the file path (only when the first case is not set)
	 */
	private static function get_js(){
		$pathFile = get_stylesheet_directory() . "/plugins/" . static::$name . "/script/" . static::$name . ".js";
		$file     = get_stylesheet_directory_uri() . "/plugins/" . static::$name . "/script/" . static::$name . ".js";
		if(!file_exists($pathFile)){
			$file = apply_filters(static::$name . '-script-url', plugins_url(null, static::$file) . "/script/" . static::$name . ".js");
		}
		return $file;
	}

	/**
	 * This function have been made to allow theme to override admin's css path of the plugin
	 * @return string
	 * Get the admin css file path of the plugin : By default it is located in {current path}/style/dlbi-hreflang.css
	 * can be overridden in 2 ways
	 * - by adding a file in {current theme path}/plugins/dlbi-hreflang/style/dlbi-hreflang.css (1)
	 * - by using the dlbi-hreflang-style-url filter witch override the file path (only when the first case is not set)
	 */
	private static function get_css(){
		$pathFile = get_stylesheet_directory() . "/plugins/" . static::$name . "/style/" . static::$name . ".css";
		$file     = get_stylesheet_directory_uri() . "/plugins/" . static::$name . "/style/" . static::$name . ".css";
		if(!file_exists($pathFile)){
			$file = apply_filters(static::$name . '-style-url', plugins_url(null, static::$file) . "/style/" . static::$name . ".css");
		}
		return $file;
	}

	public function enqueue_assets(){
		if(is_admin() && apply_filters(static::$name . 'include_scripts', true)){
			//if js or css are defined in current theme, they would override plugin's own js or css
			wp_enqueue_style(static::$name, static::get_css(), [], time());
			wp_enqueue_script(static::$name, static::get_js(), ["jquery"], time());
		}
	}


	private function checkAlternate($html){
		$pos   = strpos($html, "<link rel='alternate");
		$pos_a = strpos($html, '<link rel="alternate');
		if($pos === false && $pos_a === false){
			return false;
		}
		return true;

	}

	private function get_html($url, $post = null){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		if(!empty($post)){
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		}
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}

	public function get_href_lang_from_page(){
		$ret = [];
		if(isset($_REQUEST["url"])){
			$html = $this->get_html($_REQUEST["url"]);
			if($this->checkAlternate($html)){
				$ret["checkAlternate"] = true;
				$doc                   = new DOMDocument();
				$doc->loadHTML($html);
				$dom_list     = $doc->getElementsByTagName("link");
				$ret["items"] = [];

				$length            = $dom_list->length;
				$ret["itemsCount"] = $length;
				//				$ret["dom_list"]   = var_export($dom_list,1);
				for($i = 0;$i < $length;++ $i){
					$node     = $dom_list->item($i);
					$rel      = $node->getAttribute("rel");
					$hreflang = $node->getAttribute("hreflang");
					if($rel === "alternate" && $hreflang){
						$item             = [];
						$item["href"]     = $node->getAttribute("href");
						$item["hreflang"] = $hreflang;
						$ret["items"][]   = $item;
					}
				}
			}
		}
		echo json_encode($ret);
		//		echo $html;
		die();
	}

	public function debug($content){
		if(function_exists("dlbi_display_debug")){
			$js  = static::getJS();
			$css = static::getCSS();

			dlbi_display_debug($js, 0, "orangered");
			dlbi_display_debug($css, 0, "darkorchid");
		}

		return $content;
	}

	public function add_lang(){
		global $post;
		$post_id        = $post->ID;
		$count_repeater = get_post_meta($post_id, "dlbi_hreflang_repeater", true);
		$html           = "";
		for($i = 0;$i < $count_repeater;++ $i){
			$hreflang = get_post_meta($post_id, "dlbi_hreflang_repeater_$i" . "_dlbi_hreflang", true);
			$href     = get_post_meta($post_id, "dlbi_hreflang_repeater_$i" . "_dlbi_href", true);
			$html     .= "<link rel='alternate' hreflang='$hreflang' href='$href' />";
		}
		echo $html;
	}
}

DlbiHreflang::instance();