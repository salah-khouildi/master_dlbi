<?php

class SodexoWidget_OnlineAccreditationForm {

    // Params for display inputs in widget back-office
    var $params = array();
    // Params of widget
    var $widget_params = array();
    // Display an array of custom posts
    var $posts_lists = array();
    // Widget name
    var $name = 'Sodexo Online Accreditation Form';
    // Widget slug
    var $base = 'sodexo_online_accreditation_form';
    // Widget category
    var $category = 'Sodexo';
    // Widget class
    var $class = 'sodexo-online-accreditation-form';
    // Widget description
    var $description = 'Display online accreditation form';
    // Widget view
    var $view = SOD_VCWID_DIR . '/views/online-accreditation-form.php';

    function __construct() {
	$this->sodexo_set_widget_params();
	$this->lbisodexo_set_widget_params();
	// Hooks for grid builder
	add_shortcode($this->base, array($this, 'sodexo_grid_render'));
	add_filter('vc_grid_item_shortcodes', array($this, 'sodexo_widget_add_grid_shortcodes'));

	//Hooks for element
	add_action('vc_before_init', array($this, 'sodexo_widget_add_element'));
	add_shortcode($this->base, array($this, 'sodexo_element_render'));
    }

    /**
     * Set widget params
     */
    function lbisodexo_set_widget_params() {
	$this->widget_params = array(
	    'name' => __($this->name, 'js_composer'),
	    'base' => $this->base,
	    'class' => $this->class,
	    'category' => __($this->category, "js_composer"),
	    'description' => __($this->description, 'js_composer'),
	    'params' => $this->params,
	    'icon' => 'http://fr.sodexo.com/modules/sodexocom-templates/img/sodexo-favicon.ico'
	);
    }

    /**
     * Sets params for element and grid builder widget settings
     */
    function sodexo_set_widget_params() {
	$this->params = array(
	    // params group
	    array(
		"type" => "textfield",
		"heading" => __("Title", "js_composer"),
		"param_name" => "title",
	    ),
	    array(
		"type" => "textfield",
		"heading" => __("Description", "js_composer"),
		"param_name" => "description",
	    ),
	    array(
		"type" => "textfield",
		"heading" => __("TypeForm URL", "js_composer"),
		"param_name" => "typeform-url",
	    ),
	    array(
		"type" => "textfield",
		"heading" => __("TypeForm width", "js_composer"),
		"param_name" => "typeform-width",
	    ),
	    array(
		"type" => "textfield",
		"heading" => __("TypeForm height", "js_composer"),
		"param_name" => "typeform-height",
	    )
		)
	;
    }

    /**
     * Add widget to grid builder
     *
     * @param array $shortcodes
     * @return type
     */
    public function sodexo_widget_add_grid_shortcodes($shortcodes) {
	$params = $this->widget_params;
	$params['post_type'] = Vc_Grid_Item_Editor::postType();

	$shortcodes[$this->base] = $params;

	return $shortcodes;
    }

    /**
     * Add to posts, pages ...
     */
    public function sodexo_widget_add_element() {
	// Map the block with vc_map()
	vc_map($this->widget_params);
    }

    /**
     * Grid output function
     *
     * @param type $atts
     */
    public function sodexo_grid_render($atts) {
	SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
    }

    /**
     * Element output function
     *
     * @param type $atts
     */
    public function sodexo_element_render($atts) {
	SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
    }

}

new SodexoWidget_OnlineAccreditationForm();
