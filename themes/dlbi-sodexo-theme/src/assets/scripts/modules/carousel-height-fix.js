(function fixHeight() {
    var $items = jQuery('.force-eq-height'),
        heights = [],
        tallest;

    if ($items.length) {

        var normalizeHeights = function () {
            $items.each(function() {
                jQuery(this).css('height',''); //Remove the height defined previously (usefull if resize)
                heights.push(jQuery(this).height());
            });
            if(jQuery( window ).width() >= 1000) {
              tallest = Math.max.apply(null, heights);
              $items.each(function() {
                  jQuery(this).css('height',tallest + 'px');
              });
            }
        }

        normalizeHeights();

        jQuery(window).on('resize orientationchange', function () {
            tallest = 0, heights.length = 0;
            normalizeHeights();
        });
    }
})();
