import React from 'react'
import PropTypes from 'prop-types'
import update from 'immutability-helper'

import Counter from './../../Counter/Counter'


export default class Product extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      data: this.props.data
    }

    // console.log(this.state);
    this.productValueChange = this.productValueChange.bind(this)
    this.productSecondValueChange = this.productSecondValueChange.bind(this)
    this.onCloseClick = this.onCloseClick.bind(this)
  }

  componentWillMount () {
    this.state.data.total = this.state.data.secondValue.value * this.state.data.firstValue.value
  }

  onCloseClick (event) {
    event.preventDefault()
    this.props.onCloseClick()
  }

  productValueChange (value) {
    const firstValue = update(this.state.data.firstValue, { $merge: { value: value } })
    const data = update(this.state.data, { $merge: { firstValue: firstValue } })
    this.setState({ data: data }, () => {
      this.props.onChange(data)
    })
  }

  productSecondValueChange (value) {
    const secondValue = update(this.state.data.secondValue, { $merge: { value: value } })
    const data = update(this.state.data, { $merge: { secondValue: secondValue } })
    this.setState({ data: data }, () => {
      this.setState({ data: data }, () => {
        this.props.onChange(data)
      })
    })
  }

  render () {

    this.state.data.total = this.state.data.secondValue.value * this.state.data.firstValue.value

    if (this.state.data.type === 'foodPass') {
      this.state.data.total = this.state.data.secondValue.value * this.state.data.firstValue.value * 20
    }

    let valueFormat = this.state.data.total.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' })

    return (
      <div id='product' className={'product ' + this.state.data.title.replace(/\s+/g, '-').toLowerCase()}>

        <div className='product__header'>
          <p className='product-title'>{this.state.data.title}</p>
          <a className='product-close' href='#' onClick={this.onCloseClick}><i className='icon-close'></i></a>
        </div>

        <div className='product__content'>
          <div className='product-value'>
            <p className='product-label'>{this.state.data.firstValue.title}</p>
            <Counter
              value={this.state.data.firstValue.value}
              min={this.state.data.firstValue.min}
              max={this.state.data.firstValue.max}
              step={this.state.data.firstValue.step}
              suffix={'€'}
              onChange={(value) => { this.productValueChange(value) }}
            />
          </div>

          <div className='product-value'>
            <p className='product-label'>{this.state.data.secondValue.title}</p>
            <Counter
              value={this.state.data.secondValue.value}
              min={this.state.data.secondValue.min}
              max={this.state.data.secondValue.max}
              step={this.state.data.secondValue.step}
              onChange={(value) => { this.productSecondValueChange(value) }}
            />
          </div>

          <div className='product-value product-total'>
            <p className='product-label'>Total</p>
            <p className='product-result'>{ valueFormat }</p>
          </div>
        </div>
      </div>
    )
  }
}

Product.propTypes = {
  data: PropTypes.object,
  onChange: PropTypes.func,
  onCloseClick: PropTypes.func
}

Product.defaultProps = {
  data: {}
}
