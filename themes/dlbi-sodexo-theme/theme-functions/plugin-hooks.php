<?php
if(!function_exists('write_log')){
	function write_log($log){
		if(is_array($log) || is_object($log)){
			error_log(print_r($log, true));
		}else{
			error_log($log);
		}
	}
}


function before_imagify_optimize_attachment($id){
	write_log("before_imagify_optimize_attachment: $id");
	if(class_exists("Imagify_Attachment")){
		$attachment = new Imagify_Attachment($id);
		$attachment_path = $attachment->get_original_path();
		write_log($attachment_path);
		$pathExploded  = explode("/", $attachment_path);
		$incremental   = "";
		$uploadsPassed = false;
		foreach($pathExploded as $item){
			$hasExt = strpos($item, ".");
			if($item === "uploads"){
				$uploadsPassed = true;
				$item          .= "/backup";
			}
			if($uploadsPassed && $hasExt === false){
				write_log("before_imagify_optimize_attachment checkDir: " . $incremental . $item . " " . is_dir($incremental . $item));
				if(!is_dir($incremental . "/" . $item)){
					$created = mkdir($incremental . "/" . $item, 0755);
					write_log("before_imagify_optimize_attachment mkdir: " . $incremental . $item . " " . $created);
				}
			}
			$incremental .= $item . "/";
		}
	}
}

add_action("before_imagify_optimize_attachment", "before_imagify_optimize_attachment", 1);