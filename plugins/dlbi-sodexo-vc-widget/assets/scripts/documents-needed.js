(function($) {

  $('#document-needed-selector').change(function(){
    $('.type-contents').hide();
    $('#back-document-needed').show();
    $('#select-document-needed').hide();
    $('.info-blocks_pretitle-title').hide();
    $('#' + $(this).val()).show();
  });

  $(document).on('click', '#back-document-needed', function(event) {
        event.preventDefault();
        $('#select-document-needed').show();
        $('#back-document-needed').hide();
        $('.type-contents').hide();
        $('.info-blocks_pretitle-title').show();
    });


})(jQuery);
