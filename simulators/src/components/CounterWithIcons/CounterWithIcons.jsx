import React from 'react'
import PropTypes from 'prop-types'
import update from 'immutability-helper'

import Tooltip from 'rc-tooltip'

import Counter from '../Counter/Counter'

import './CounterWithIcons.scss'

export default class CounterWithIcons extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      className: this.props.className,
      icons: this.props.icons,
      text: this.props.text,
      label: this.props.label,
      value: this.props.value,
      min: this.props.min,
      max: this.props.max,
      step: this.props.step,
      stepInput: this.props.stepInput,
      suffix: this.props.suffix,
      prefix: this.props.prefix,
      currency: this.props.currency || false,
      tooltip: this.props.tooltip,
    }

    this.valueChange = this.valueChange.bind(this)
  }

  componentWillMount () {
    this.iconChange()
    this.forceUpdate()
  }

  iconChange () {
    let length = this.state.icons.length
    let value = this.state.value
    let min = this.state.min
    let max = this.state.max

    for (let i = 1; i <= length; i++) {
      if ((value - min) <= (((max - min) / length) * i)) {
        let icon = this.state.icons[i - 1]
        let type = icon.substring(11, 14)

        if (type === 'svg') {
          let base64 = icon.substring(0, 26)
          let encoded = icon.substring(26)
          let decoded = atob(encoded)
          // Create an HTML element from decoded SVG
          let wrapper = document.createElement('div')
          wrapper.innerHTML = decoded
          let newSvg = wrapper.getElementsByTagName('svg')[0]
          // Lookup the <path> and get a ref
          var paths = newSvg.getElementsByTagName('path');
          [].forEach.call(paths, function (path) {
            path.setAttribute('fill', '#ed424a')
            path.setAttribute('stroke', '#ed424a')
            path.setAttribute('style', 'fill:#ed424a')
            path.setAttribute('style', 'stroke:#ed424a')
          })

          // Show modified image
          var s = new XMLSerializer().serializeToString(newSvg)
          encoded = btoa(s)
          icon = base64 + encoded
          return this.setState({ icon: icon })
        } else {
          return this.setState({ icon: icon })
        }
      }
    }
  }

  valueChange (value) {
    this.setState({ value: value }, () => {
      this.iconChange()
      this.props.onChange(value)
    })
  }

  render () {

    let moreClass = !this.props.label ? '' : ' label-after'
    let noTooltip = this.props.tooltip !== '' ? '' : ' hide-tooltip'

    return (
      <div className={'counter_with_icons ' + this.props.className.replace(/\s+/g, '-').toLowerCase()}>

        <Tooltip
          overlayClassName={'counter_with_icons-tooltipOverlay' + noTooltip}
          placement='right'
          trigger={['click', 'hover']}
          overlay={this.state.tooltip}>
          <a href='#' className={'counter_with_icons-tooltipLink' + noTooltip}></a>
        </Tooltip>

        <img className='counter_with_icons-icon' src={this.state.icon} alt={this.props.text} />

        {/* <div
          className='counter_with_icons-icon'
          style={{ backgroundImage: `url(${this.props.icons})` }}>
        </div> */}
        <p className={'counter_with_icons-text' + moreClass}>{this.props.text}</p>
        <p className='counter_with_icons-label'>{this.props.label}</p>

        <Counter
          value={this.props.value}
          min={this.props.min}
          max={this.props.max}
          step={this.props.step}
          stepInput={this.props.stepInput}
          prefix={this.props.prefix}
          suffix={this.props.suffix}
          currency={this.props.currency}
          onChange={(value) => { this.valueChange(value) }}
        />
      </div>
    )
  }
}

CounterWithIcons.propTypes = {
  onChange: PropTypes.func,
  className: PropTypes.any,
  icons: PropTypes.any,
  text: PropTypes.any,
  label: PropTypes.any,
  value: PropTypes.any,
  min: PropTypes.any,
  max: PropTypes.any,
  step: PropTypes.any,
  stepInput: PropTypes.any,
  suffix: PropTypes.any,
  prefix: PropTypes.any,
  currency: PropTypes.bool,
  tooltip: PropTypes.any,

}

CounterWithIcons.defaultProps = {
  icons: '',
  text: '',
  label: '',
  value: '',
  min: '',
  max: '',
  step: '',
  stepInput: '',
  suffix: '',
  prefix: '',
  currency: false,
  tooltip: ''
}
