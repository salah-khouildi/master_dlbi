<?php

class SodexoWidget_SingleCarousel {

    // Params for display inputs in widget back-office
    var $params = array();
    // Params of widget
    var $widget_params = array();
    // Display an array of custom posts
    var $posts_lists = array();
    // Widget name
    var $name = 'Single Carousel';
    // Widget slug
    var $base = 'single_carousel';
    // Widget category
    var $category = 'Sodexo';
    // Widget class
    var $class = 'single-carousel';
    // Widget description
    var $description = 'Single Carousel.';
    // Widget view
    var $view = SOD_VCWID_DIR . '/views/single-carousel.php';

    function __construct() {
        $this->sodexo_set_widget_params();
        $this->lbisodexo_set_widget_params();
        // Hooks for grid builder
        add_shortcode($this->base, array($this, 'sodexo_grid_render'));
        add_filter('vc_grid_item_shortcodes', array($this, 'sodexo_widget_add_grid_shortcodes'));

        //Hooks for element
        add_action('vc_before_init', array($this, 'sodexo_widget_add_element'));
        add_shortcode($this->base, array($this, 'sodexo_element_render'));
    }

    /**
     * Get posts list
     *
     * @param type $post_type
     */
    function sodexo_get_posts_list($post_type) {
       $this->posts_lists = SodexoVcwid_Client::sodexo_vcwid_get_posts_values($post_type);
    }

    /**
     * Set widget params
     */
    function lbisodexo_set_widget_params() {
        $this->widget_params = array(
            'name' => __($this->name, 'js_composer'),
            'base' => $this->base,
            'class' => $this->class,
            'category' => __($this->category, "js_composer"),
            'description' => __($this->description, 'js_composer'),
            'params' => $this->params,
            'icon'  => 'http://fr.sodexo.com/modules/sodexocom-templates/img/sodexo-favicon.ico'
        );
    }

    /**
     * Sets params for element and grid builder widget settings
     */
    function sodexo_set_widget_params() {
        $this->params = array(
            // params group
            array(
                'heading' => __('Titles', 'js_composer'),
                'type' => 'param_group',
                'value' => '',
                'param_name' => 'titles',
                // Note params is mapped inside param-group:
                'params' => array(
                    array(
                        "type" => "textfield",
                        "holder" => "",
                        "class" => "",
                        "heading" => __('Value', 'js_composer'),
                        "param_name" => 'value',
                        "value" => ''
                    ),
                    array(
                        "type" => "textfield",
                        "holder" => "",
                        "class" => "",
                        "heading" => __('Unit', 'js_composer'),
                        "param_name" => 'unit',
                        'description' => __('Enter a unit as % for example.', 'js_composer'),
                        "value" => ''
                    ),
                    array(
                        "type" => "textfield",
                        "holder" => "",
                        "class" => "",
                        "heading" => __('Description', 'js_composer'),
                        "param_name" => 'description',
                        "value" => ''
                    ),
                    array(
                        "type" => "vc_link",
                        "heading" => __('Link', 'js_composer'),
                        "param_name" => 'link',
                        "value" => ''
                    ),
                ) 
            ),
        );
    }

    /**
     * Add widget to grid builder
     *
     * @param array $shortcodes
     * @return type
     */
    public function sodexo_widget_add_grid_shortcodes($shortcodes) {
        $params = $this->widget_params;
        $params['post_type'] = Vc_Grid_Item_Editor::postType();

        $shortcodes[$this->base] = $params;

        return $shortcodes;
    }

    /**
     * Add to posts, pages ...
     */
    public function sodexo_widget_add_element() {
        // Map the block with vc_map()
        vc_map($this->widget_params);
    }

    /**
     * Grid output function
     *
     * @param type $atts
     */
    public function sodexo_grid_render($atts) {
        SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
    }

    /**
     * Element output function
     *
     * @param type $atts
     */
    public function sodexo_element_render($atts) {
        SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
    }

}
new SodexoWidget_SingleCarousel();
