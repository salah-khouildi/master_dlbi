<?php
/*
  Plugin Name: Sodexo Chili simulator
  Description: Plugin for Chili simulator
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: lbi-sodexo-theme
 */


// Plugin Consts
define('SOD_CHI_VERSION', '1.0');
define('SOD_CHI_URL', plugins_url('', __FILE__));
define('SOD_CHI_DIR', dirname(__FILE__));

//Load class
require( SOD_CHI_DIR . '/inc/class-client.php' );
require( SOD_CHI_DIR . '/inc/class-admin.php' );
require( SOD_CHI_DIR . '/inc/widgets/class-chili-simulator.php' );

//Init
function dlbi_sodexo_chili_sim_init() {
    new SodexoChiliSimulator_Client();
    new SodexoWidget_ChiliSimulator();

    if(is_admin()){
	new SodexoChiliSimulator_Admin();
    }

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-sodexo-chili-simulator', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'dlbi_sodexo_chili_sim_init', 11);
