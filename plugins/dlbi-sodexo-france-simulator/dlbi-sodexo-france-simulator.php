<?php

/*
  Plugin Name: Sodexo french simulator
  Description: Plugin for France simulator
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-sodexo-french-simulator
 */

// require 'plugin-update-checker/plugin-update-checker.php';
// $MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
//     'https://wp-update.dev.soxodlbi.xyz/?action=get_metadata&slug=dlbi-sodexo-france-simulator', //Metadata URL.
//     __FILE__, //Full path to the main plugin file.
//     'dlbi-sodexo-french-simulator' //Plugin slug. Usually it's the same as the name of the directory.
// );

// Plugin Consts
define('SOD_FRE_VERSION', '1.0');
define('SOD_FRE_URL', plugins_url('', __FILE__));
define('SOD_FRE_DIR', dirname(__FILE__));

//Load class
require( SOD_FRE_DIR . '/inc/class-client.php' );
require( SOD_FRE_DIR . '/inc/class-admin.php' );
require( SOD_FRE_DIR . '/inc/widgets/class-french-simulator.php' );

//Init
function dlbi_sodexo_france_sim_init() {
    new SodexoFranceSimulator_Client();
    new SodexoWidget_FrenchSimulator();
    if(is_admin()){
	new SodexoFranceSimulator_Admin();
    }
    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-sodexo-french-simulator', false, dirname(plugin_basename(__FILE__)) . '/languages/');

}

add_action('plugins_loaded', 'dlbi_sodexo_france_sim_init', 11);
