<?php
$documents_needed = vc_param_group_parse_atts( $atts['documents_needed_titles'] );
// dlbi_display_debug( $documents_needed, 0, "orange" );
?>

<section class=" alt choose-sodexo">
	<div class="col-12 choose-sodexo--two_blocks">
		<div class="choose-sodexo--container bg-grey info-blocks">

			<?php if ( count( $documents_needed ) !== 1 ) : ?>
				<a href="#" id="back-document-needed"><i class="fa fa-arrow-left fa-lg" aria-hidden="true"></i><?php echo __( 'Back', 'dlbi-sodexo-vc-widget' ); ?></a>
			<?php endif ?>

			<?php if ( array_key_exists( 'documents_needed_icon', $atts ) ) : ?>
				<div class="info-blocks_icon">
					<?php
					// Get image, title, label and link : 'READ MORE'
					$image = $atts['documents_needed_icon'];
					if ( ! empty( $image ) ) :
						echo wp_get_attachment_image( $image );
					endif;
					?>
				</div>
			<?php endif ?>

			<?php if ( array_key_exists( 'widget_title', $atts ) ) : ?>
				<div class="info-blocks_subtitle">
					<h3 class="sodexo-title info-blocks_title"><?php echo $atts['widget_title']; ?></h3>
				</div>
			<?php endif; ?>

			<?php if ( array_key_exists( 'widget_subtitle', $atts ) ) : ?>
				<div class="info-blocks_subtitle">
					<p class="info-blocks_pretitle info-blocks_pretitle-title"><?php echo $atts['widget_subtitle']; ?></p>
				</div>
			<?php endif; ?>

			<?php if ( $documents_needed && count( $documents_needed ) !== 1 ) : ?>
				<div id="select-document-needed" class="select">
					<div class="select-style">
						<select id="document-needed-selector">
							<option selected="selected" disabled><?php echo __( 'Select your business type', 'dlbi-sodexo-vc-widget' ); ?></option>
							<?php foreach ( $documents_needed as $document ) : ?>
								<option value="doc<?php echo rawurldecode( sanitize_title( $document['documents_needed_type_of_business'] ) ); ?>"><?php echo $document['documents_needed_type_of_business']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			<?php endif ?>

			<?php foreach ( $documents_needed as $document ) : ?>
				<div class="type-contents" id="doc<?php echo rawurldecode( sanitize_title( $document['documents_needed_type_of_business'] ) ); ?>" style="<?php count( $documents_needed ) === 1 && print_r( 'display: block;' ); ?>">
					<p class="info-blocks_pretitle info-blocks_pretitle-select"><?php echo apply_filters( 'dlbi_tel_link', $document['documents_needed_subtitle'] ); ?></p>
					<ul>
						<?php
						if ( array_key_exists( 'documents_needed_lines', $document ) ) :
							$needed = vc_param_group_parse_atts( $document['documents_needed_lines'] );
							if ( $needed ) :
								foreach ( $needed as $lines ) :
									if ( array_key_exists( 'documents_needed_line_of_text', $lines ) ) :
										?>
										<li>
											<?php echo apply_filters( 'dlbi_tel_link', $lines['documents_needed_line_of_text'] ); ?>
										</li>
									<?php endif ?>
								<?php endforeach; ?>
							<?php endif ?>
						<?php endif; ?>
					</ul>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
