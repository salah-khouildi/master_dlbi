<?php

class SodexoLuxembourgSimulator_Client {

    public function __construct() {
	add_filter('acf/settings/save_json', array($this, 'sodexo_acf_json_save_point'));
	add_filter('acf/settings/load_json', array($this, 'sodexo_acf_json_load_point'));
    }

    public function sodexo_acf_json_save_point($path) {

	// update path
	$path = SOD_LUX_DIR . '/acf-json';
	// return
	return $path;
    }

    public function sodexo_acf_json_load_point($paths) {

	// remove original path (optional)
	unset($paths[0]);


	// append path
	$paths[] = SOD_LUX_DIR . '/acf-json';


	// return
	return $paths;
    }
    /**
     *  output function
     *
     * @param type $atts
     * @param type $path
     */
    public static function sodexo_widget_render($atts, $path) {
        include($path);
    }
}
