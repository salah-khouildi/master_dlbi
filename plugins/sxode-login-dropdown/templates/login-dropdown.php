<div class="<?php echo $menu_type ?>-menu-login-item dropdown loginDropdown">
    <a title="<?php echo __('Login', 'lbi-sodexo-theme'); ?>"
       href="#"
		<?php if($menu_type === 'combined'): ?>
            class="icon-person loginDropdown__link"
		<?php else: ?>
            class="btn-sodexo btn-sodexo-white icon-person dropdown-toggle loginDropdown__link"
		<?php endif; ?>
       role="button"
       id="dropdownMenuButton"
       data-toggle="dropdown"
       aria-haspopup="true"
       aria-expanded="false"
    >
		<?php
		echo ($menu_type === 'combined') ? '<span class="sr-only">' : '<span class="hidden-md-down">';
		echo __('Login', 'lbi-sodexo-theme');
		echo ($menu_type === 'combined') ? '</span>' : '</span>';
		?>
    </a>

    <div class="dropdown-menu loginDropdown__menu" aria-labelledby="dropdownMenuButton">
        <div class="loginDropdown__header">
            <h5 class="loginDropdown__title"><?php echo get_field('sxode_login_dropdown_caption', 'option') ?></h5>
        </div>

		<?php if(get_field('sxode_login_dropdown_introduction', 'option')): ?>
            <div class="loginDropdown__introduction">
				<?php echo get_field('sxode_login_dropdown_introduction', 'option') ?>
            </div>
		<?php endif; ?>

		<?php if(have_rows('sxode_login_dropdown_links', 'option')): ?>
			<?php while(have_rows('sxode_login_dropdown_links', 'option')): the_row(); ?>

				<?php if(get_sub_field('type') === 'heading'): ?>
                    <h5 class="loginDropdown__heading">
						<?php echo get_sub_field('title') ?>
                    </h5>
				<?php else: ?>
                    <a
                            class="dropdown-item loginDropdown__item <?php echo ($menu_type === 'combined') ? 'loginDropdown__item--unsetCombinedSettings' : '' ?>"
                            href="<?php echo get_sub_field('url'); ?>"
						<?php if(get_field('sxode_login_dropdown_target_blank', 'option')): ?>
                            target="_blank"
						<?php endif; ?>
                    >
                        <i class="loginDropdown__arrow fa fa-arrow-right fa-move" aria-hidden="true"></i><?php echo get_sub_field('title') ?>
                    </a>
				<?php endif; ?>

			<?php endwhile; ?>
		<?php endif; ?>
    </div>
</div>