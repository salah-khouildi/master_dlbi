<?php
//List of attributs
$titles = vc_param_group_parse_atts($atts['titles']);
$affContainer = (array_key_exists('affiliate_wide_format', $atts) && $atts['affiliate_wide_format']) ? "container" : "";

?>
<section class=" alt choose-sodexo affiliate-info <?php echo $affContainer ?>">
    <div class="choose-sodexo--two_blocks col-12">
    <?php
    // Title : A selection of parteners
    if (array_key_exists('bloc_title', $atts)): ?>
        <p class="sodexo-pretitle"><?php echo $atts['bloc_title']; ?></p>
    <?php endif ?>


    <?php if (array_key_exists('titles', $atts)) : ?>
        <?php
            $affCols = (array_key_exists('affiliate_wide_format', $atts) && $atts['affiliate_wide_format']) ? "" : "col-md-6";
        ?>
        <div class="choose-sodexo--container bg-grey">
            <div class="lmt-contact">
                <div class="lmt-contact-info">
                    <article>
                        <?php

                        //Repeated block
                        foreach ($titles as $title):
                        ?>

                            <?php if (array_key_exists('affiliate_bloc_icon_1', $title)): ?>
                                <span class="assistance-icon">
                                    <?php
                                    // Get image, title, label and link : 'READ MORE'
                                    $image = $title['affiliate_bloc_icon_1'];
                                    if (!empty($image)):
                                        echo wp_get_attachment_image($image);
                                    endif;
                                    ?>
                                </span>
                            <?php endif ?>

                            <?php if (array_key_exists('affiliate_bloc_title', $title)): ?>
                                <h3 class="sodexo-title"><?php echo $title['affiliate_bloc_title']; ?></h3>
                            <?php endif ?>

                            <?php if (array_key_exists('affiliate_bloc_subtitle', $title)): ?>
                                <?php echo $title['affiliate_bloc_subtitle']; ?>
                            <?php endif ?>

                            <?php if (array_key_exists('affiliate_bloc_phone', $title)): ?>
                                <a href="tel:<?php echo $title['affiliate_bloc_phone']; ?>"><?php echo $title['affiliate_bloc_phone']; ?></a>
                            <?php endif ?>


                            <?php if (array_key_exists('affiliate_bloc_icon_2', $title)  || array_key_exists('affiliate_Link_after_icon_2',  $title)): ?>
                                <div class="lmt-contact-email">
                            <?php endif ?>

                            <?php if (array_key_exists('affiliate_bloc_icon_2', $title)): ?>
                                <?php
                                // Get image, title, label and link : 'READ MORE'
                                $image = $title['affiliate_bloc_icon_2'];
                                if (!empty($image)):
                                    echo wp_get_attachment_image($image);
                                endif;
                                ?>
                            <?php endif ?>

                            <?php
                            if (array_key_exists('affiliate_Link_after_icon_2',  $title)): ?>
                                <?php $linkField = vc_build_link( $title['affiliate_Link_after_icon_2']); ?>
                                <a href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if ($linkField['target']) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
                            <?php endif;?>

                            <?php if (array_key_exists('affiliate_bloc_icon_2', $title)  || array_key_exists('affiliate_Link_after_icon_2',  $title)): ?>
                                </div>
                            <?php endif ?>
                        <?php endforeach; ?>
                    </article>
                </div>
            </div>

        </div>


        <?php
        if (array_key_exists('affiliate_wide_format', $atts) && $atts['affiliate_wide_format']) :
            if (array_key_exists('affiliate_pre_text_link', $atts) || array_key_exists('affiliate_link',  $atts) ) :
        ?>
        <div class="text-center mt-5 mb-5">

            <?php if (array_key_exists('affiliate_pre_text_link', $atts)) : ?>
                <p><strong><?php echo $atts['affiliate_pre_text_link']; ?></strong></p>
            <?php endif; ?>

            <?php if (array_key_exists('affiliate_link',  $atts)): ?>
                <?php $linkField = vc_build_link( $atts['affiliate_link']); ?>
                <a class="btn-sodexo btn-sodexo-white"  href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if ($linkField['target']) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
            <?php endif;?>
        </div>
        <?php
            endif;
        endif;
     endif;?>
    </div>

</section>
