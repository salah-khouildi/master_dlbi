<?php

// Load WordPress
$bootstrap = 'wp-load.php';
while (!is_file($bootstrap)) {
    if (is_dir('..'))
	chdir('..');
    else
	die('EN: Could not find WordPress! FR : Impossible de trouver WordPress !');
}
require_once( $bootstrap );

$discount_vat = get_field('discount_vat', 'option');
$reduce_taxable = get_field('reduce_net_result_taxable', 'option');
$days_per_month = get_field('days_per_month', 'option');

if (isset($_POST['nb-employees']) && isset($_POST['daily-amount'])):
    $cost_without_cheque_restaurant = (int) $_POST['nb-employees'] * (int) $_POST['daily-amount'] * (int) $days_per_month;
    $discount_without_vat = round(-$cost_without_cheque_restaurant + ((int) $cost_without_cheque_restaurant / (float) (1 . '.' . $discount_vat)));
    $net_vat_cost = $cost_without_cheque_restaurant + $discount_without_vat;
    $reduce_net_result_taxable = round(-$net_vat_cost * (float) ($reduce_taxable / 100));
    $real_cost = $net_vat_cost + $reduce_net_result_taxable;
    $daily_saving = $cost_without_cheque_restaurant - $real_cost;
    $percent_of_savings = round($daily_saving / $cost_without_cheque_restaurant * 100, 2);
    $cost_without_cheque_restaurant_annual = ((int) $_POST['nb-employees'] * (int) $_POST['daily-amount'] * (int) $days_per_month) * 12;
    $discount_without_vat_annual = round(-$cost_without_cheque_restaurant_annual + ((int) $cost_without_cheque_restaurant_annual / (float) (1 . '.' . $discount_vat)));
    $net_vat_cost_annual = $cost_without_cheque_restaurant_annual + $discount_without_vat_annual;
    $reduce_net_result_taxable_annual = round(-$net_vat_cost_annual * (float) ($reduce_taxable / 100));
    $real_cost_annual = $net_vat_cost_annual + $reduce_net_result_taxable_annual;
    $annual_saving = $cost_without_cheque_restaurant_annual - $real_cost_annual;
    $percent_of_savings_annual = round($annual_saving / $cost_without_cheque_restaurant_annual * 100, 2);

    $return = array(
	'cost_without_cheque_restaurant' => $cost_without_cheque_restaurant,
	'discount_without_vat' => $discount_without_vat,
	'net_vat_cost' => $net_vat_cost,
	'reduce_net_result_taxable' => $reduce_net_result_taxable,
	'real_cost' => $real_cost,
	'daily_saving' => $daily_saving,
	'percent_of_savings' => $percent_of_savings,
	'cost_without_cheque_restaurant_annual' => $cost_without_cheque_restaurant_annual,
	'discount_without_vat_annual' => $discount_without_vat_annual,
	'net_vat_cost_annual' => $net_vat_cost_annual,
	'reduce_net_result_taxable_annual' => $reduce_net_result_taxable_annual,
	'real_cost_annual' => $real_cost_annual,
	'annual_saving' => $annual_saving,
	'percent_of_savings_annual' => $percent_of_savings_annual,
    );
    wp_send_json($return);

endif;
