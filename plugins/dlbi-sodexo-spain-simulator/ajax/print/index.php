<?php
// Load WordPress
$wp_load = $_SERVER['DOCUMENT_ROOT'] . '/wp/wp-load.php';
require_once( $wp_load );

$simulator;
$numberofemployee;
$vouchervalue;
$salary;
$retencionirpfmedia;
$retencionirpfticket;
$taxessaved;
$companyspent;
$employeessave;
$nombresalaries;
$nombreticketsresto;
$valeurtitre;
$valeurparticipationemployeur;
$budgettotal;
$participationemployeur;
$cofinancement;
$pouvoirachatadditionnel;
$economiesrealiseesemployeur;
$gainemployee;
$declarationcommune;
$nbrchild;
$nbrchildunderthree;
$salairebrut;
$totaltaxwithoutsalarysacrifice;
$totaltaxwithsalarysacrifice;
$totalfacialbenefits;
$eurosave;
$foodpassimporte;
$foodpassmonth;
$foodpasstotal;
$transportpassimporte;
$transportpassmonth;
$transportpasstotal;
$childcarepassimporte;
$childcarepassmonth;
$childcarepasstotal;
$healthpassimporte;
$healthpassmonth;
$healthpasstotal;
$trainingpassimporte;
$trainingpassmonth;
$foodpassselected;
$childcarepassselected;
$healthpassselected;
$trainingpassselected;
$transportpassselected;
$productname;

$logo      = get_field( 'simulator_logo', 'option' );
$link      = get_field( 'simulator_link', 'option' );
$fee_label = get_field( 'pdf_delivery_fee_label', 'option' );

if ( isset( $_GET ) && ( ! empty( $_GET ) ) ) {
	$simulatorvalue = $_GET; // Permit to change the method easily
	if ( ( array_key_exists( 'productname', $simulatorvalue ) ) && ( ! empty( $simulatorvalue['productname'] ) ) ) {
		$productname = $simulatorvalue['productname'];
	}
// spain r/c/t
	if ( ( array_key_exists( 'numberofemployee', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['numberofemployee'] ) ) && ( ! empty( $simulatorvalue['numberofemployee'] ) ) ) {
		$numberofemployee = $simulatorvalue['numberofemployee'];
	}
	if ( ( array_key_exists( 'vouchervalue', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['vouchervalue'] ) ) && ( ! empty( $simulatorvalue['vouchervalue'] ) ) ) {
		$vouchervalue = $simulatorvalue['vouchervalue'];
	}
	if ( ( array_key_exists( 'salary', $simulatorvalue ) ) && ( ! empty( $simulatorvalue['salary'] ) ) ) {
		$salary = $simulatorvalue['salary'];
	}
	if ( ( array_key_exists( 'retencionirpfmedia', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['retencionirpfmedia'] ) ) && ( ! empty( $simulatorvalue['retencionirpfmedia'] ) ) ) {
		$retencionirpfmedia = $simulatorvalue['retencionirpfmedia'];
	}
	if ( ( array_key_exists( 'retencionirpfticket', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['retencionirpfticket'] ) ) && ( ! empty( $simulatorvalue['retencionirpfticket'] ) ) ) {
		$retencionirpfticket = $simulatorvalue['retencionirpfticket'];
	}
	if ( ( array_key_exists( 'taxessaved', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['taxessaved'] ) ) && ( ! empty( $simulatorvalue['taxessaved'] ) ) ) {
		$taxessaved = $simulatorvalue['taxessaved'];
	}
	if ( ( array_key_exists( 'companyspent', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['companyspent'] ) ) && ( ! empty( $simulatorvalue['companyspent'] ) ) ) {
		$companyspent = $simulatorvalue['companyspent'];
	}
	if ( ( array_key_exists( 'employeessave', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['employeessave'] ) ) && ( ! empty( $simulatorvalue['employeessave'] ) ) ) {
		$employeessave = $simulatorvalue['employeessave'];
	}

// spain salary sacrifice
	if ( ( array_key_exists( 'simulator', $simulatorvalue ) ) && ( ! empty( $simulatorvalue['simulator'] ) ) ) {
		$simulator = $simulatorvalue['simulator'];
	}
	if ( ( array_key_exists( 'declarationcommune', $simulatorvalue ) ) && ( ! empty( $simulatorvalue['declarationcommune'] ) ) ) {
		$declarationcommune = $simulatorvalue['declarationcommune'] == 'false' ? 'No' : 'Sí';
	}
	if ( ( array_key_exists( 'nbrchild', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['nbrchild'] ) ) && ( ! empty( $simulatorvalue['nbrchild'] ) ) ) {
		$nbrchild = $simulatorvalue['nbrchild'];
	}
	if ( ( array_key_exists( 'nbrchildunderthree', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['nbrchildunderthree'] ) ) && ( ! empty( $simulatorvalue['nbrchildunderthree'] ) ) ) {
		$nbrchildunderthree = $simulatorvalue['nbrchildunderthree'];
	}
	if ( ( array_key_exists( 'salairebrut', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['salairebrut'] ) ) && ( ! empty( $simulatorvalue['salairebrut'] ) ) ) {
		$salairebrut = $simulatorvalue['salairebrut'];
	}
	if ( ( array_key_exists( 'totaltaxwithoutsalarysacrifice', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['totaltaxwithoutsalarysacrifice'] ) ) && ( ! empty( $simulatorvalue['totaltaxwithoutsalarysacrifice'] ) ) ) {
		$totaltaxwithoutsalarysacrifice = $simulatorvalue['totaltaxwithoutsalarysacrifice'];
	}
	if ( ( array_key_exists( 'totaltaxwithsalarysacrifice', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['totaltaxwithsalarysacrifice'] ) ) && ( ! empty( $simulatorvalue['totaltaxwithsalarysacrifice'] ) ) ) {
		$totaltaxwithsalarysacrifice = $simulatorvalue['totaltaxwithsalarysacrifice'];
	}
	if ( ( array_key_exists( 'totalfacialbenefits', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['totalfacialbenefits'] ) ) && ( ! empty( $simulatorvalue['totalfacialbenefits'] ) ) ) {
		$totalfacialbenefits = $simulatorvalue['totalfacialbenefits'];
	}
	if ( ( array_key_exists( 'eurosave', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['eurosave'] ) ) && ( ! empty( $simulatorvalue['eurosave'] ) ) ) {
		$eurosave = $simulatorvalue['eurosave'];
	}
	if ( ( array_key_exists( 'foodpassimporte', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['foodpassimporte'] ) ) && ( ! empty( $simulatorvalue['foodpassimporte'] ) ) ) {
		$foodpassimporte = $simulatorvalue['foodpassimporte'];
	}
	if ( ( array_key_exists( 'foodpassmonth', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['foodpassmonth'] ) ) && ( ! empty( $simulatorvalue['foodpassmonth'] ) ) ) {
		$foodpassmonth = $simulatorvalue['foodpassmonth'];
	}
	if ( ( array_key_exists( 'foodpasstotal', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['foodpasstotal'] ) ) && ( ! empty( $simulatorvalue['foodpasstotal'] ) ) ) {
		$foodpasstotal = $simulatorvalue['foodpasstotal'];
	}
	if ( ( array_key_exists( 'transportpassimporte', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['transportpassimporte'] ) ) && ( ! empty( $simulatorvalue['transportpassimporte'] ) ) ) {
		$transportpassimporte = $simulatorvalue['transportpassimporte'];
	}
	if ( ( array_key_exists( 'transportpassmonth', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['transportpassmonth'] ) ) && ( ! empty( $simulatorvalue['transportpassmonth'] ) ) ) {
		$transportpassmonth = $simulatorvalue['transportpassmonth'];
	}
	if ( ( array_key_exists( 'transportpasstotal', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['transportpasstotal'] ) ) && ( ! empty( $simulatorvalue['transportpasstotal'] ) ) ) {
		$transportpasstotal = $simulatorvalue['transportpasstotal'];
	}
	if ( ( array_key_exists( 'childcarepassimporte', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['childcarepassimporte'] ) ) && ( ! empty( $simulatorvalue['childcarepassimporte'] ) ) ) {
		$childcarepassimporte = $simulatorvalue['childcarepassimporte'];
	}
	if ( ( array_key_exists( 'childcarepassmonth', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['childcarepassmonth'] ) ) && ( ! empty( $simulatorvalue['childcarepassmonth'] ) ) ) {
		$childcarepassmonth = $simulatorvalue['childcarepassmonth'];
	}
	if ( ( array_key_exists( 'childcarepasstotal', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['childcarepasstotal'] ) ) && ( ! empty( $simulatorvalue['childcarepasstotal'] ) ) ) {
		$childcarepasstotal = $simulatorvalue['childcarepasstotal'];
	}
	if ( ( array_key_exists( 'healthpassimporte', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['healthpassimporte'] ) ) && ( ! empty( $simulatorvalue['healthpassimporte'] ) ) ) {
		$healthpassimporte = $simulatorvalue['healthpassimporte'];
	}
	if ( ( array_key_exists( 'healthpassmonth', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['healthpassmonth'] ) ) && ( ! empty( $simulatorvalue['healthpassmonth'] ) ) ) {
		$healthpassmonth = $simulatorvalue['healthpassmonth'];
	}
	if ( ( array_key_exists( 'healthpasstotal', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['healthpasstotal'] ) ) && ( ! empty( $simulatorvalue['healthpasstotal'] ) ) ) {
		$healthpasstotal = $simulatorvalue['healthpasstotal'];
	}
	if ( ( array_key_exists( 'trainingpassimporte', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['trainingpassimporte'] ) ) && ( ! empty( $simulatorvalue['trainingpassimporte'] ) ) ) {
		$trainingpassimporte = $simulatorvalue['trainingpassimporte'];
	}
	if ( ( array_key_exists( 'trainingpassmonth', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['trainingpassmonth'] ) ) && ( ! empty( $simulatorvalue['trainingpassmonth'] ) ) ) {
		$trainingpassmonth = $simulatorvalue['trainingpassmonth'];
	}
	if ( ( array_key_exists( 'foodpassselected', $simulatorvalue ) ) && ( ! empty( $simulatorvalue['foodpassselected'] ) ) ) {
		$foodpassselected = $simulatorvalue['foodpassselected'];
	}
	if ( ( array_key_exists( 'childcarepassselected', $simulatorvalue ) ) && ( ! empty( $simulatorvalue['childcarepassselected'] ) ) ) {
		$childcarepassselected = $simulatorvalue['childcarepassselected'];
	}
	if ( ( array_key_exists( 'healthpassselected', $simulatorvalue ) ) && ( ! empty( $simulatorvalue['healthpassselected'] ) ) ) {
		$healthpassselected = $simulatorvalue['healthpassselected'];
	}
	if ( ( array_key_exists( 'trainingpassselected', $simulatorvalue ) ) && ( ! empty( $simulatorvalue['trainingpassselected'] ) ) ) {
		$trainingpassselected = $simulatorvalue['trainingpassselected'];
	}
	if ( ( array_key_exists( 'transportpassselected', $simulatorvalue ) ) && ( ! empty( $simulatorvalue['transportpassselected'] ) ) ) {
		$transportpassselected = $simulatorvalue['transportpassselected'];
	}
}
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Print your simulator</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="theme-color" content="#283897">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="styles/style.css">
</head>

<body id="page">
<div class="container-fluid">
    <div class="row" id="logo">
		<?php if ( $logo ): ?>
            <div class="col-md-3"><h1><img src="<?php echo $logo['sizes']['large'] ?>" width="200" height="70" alt="Sodexo"/></h1></div>
		<?php else: ?>
            <div class="col-md-3"><h1><img src="img/logo-sodexo-header.svg" width="200" height="70" alt="Sodexo"/></h1></div>
		<?php endif; ?>
    </div>
	<?php if ( isset( $simulator ) && $simulator != 'salary' ): ?>
        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordered" id="infos">
                    <thead>
                    <tr>
                        <th class="active">Número de empleados</th>
                        <th class="active">Importe (€)</th>
                        <th class="active">Salario medio</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?php if ( ! empty( $numberofemployee ) ) {
								echo $numberofemployee;
							} ?></td>
                        <td><?php if ( ! empty( $vouchervalue ) ) {
								echo $vouchervalue;
							} ?></td>
                        <td><?php if ( ! empty( $salary ) ) {
								echo number_format( $salary, 2, ',', ' ' );
							} ?></td>
                    </tr>
                    </tbody>
                </table>
                <table class="table table-bordered" id="result">
                    <tbody>
                    <tr>
                        <td class="info"><b>Ahorro : </b></td>
                        <td class="info"><b>IRPF con <?php if ( ! empty( $productname ) ) {
									echo $productname;
								} ?>: </b></td>
                        <td><?php if ( ! empty( $retencionirpfmedia ) ) {
								echo number_format( $retencionirpfmedia, 2, ',', '&nbsp;' );
							} ?> €
                        </td>
                        <td class="info"><b>IRPFF con el equivalente en salario : </b></td>
                        <td><?php if ( ! empty( $retencionirpfticket ) ) {
								echo number_format( $retencionirpfticket, 2, ',', '&nbsp;' );
							} ?> €
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="table table-bordered" id="donut">
                    <tbody>
                    <tr>
                        <td><b>IRPF ahorrado : </b><?php if ( ! empty( $taxessaved ) ) {
								echo number_format( $taxessaved, 2, ',', ' ' );
							} ?> %
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="table table-bordered" id="economy">
                    <tbody>
                    <tr>
                        <td><b>La empresa gasta :</b> <?php if ( ! empty( $companyspent ) ) {
								echo number_format( $companyspent, 2, ',', ' ' );
							} ?> €
                        </td>


                    </tr>
                    <tr>
                        <td><b>El neto de los empleados aumenta en :</b> <?php if ( ! empty( $employeessave ) ) {
								echo number_format( $employeessave, 2, ',', ' ' );
							} ?> €
                        </td>


                    </tr>
                    </tbody>
                </table>
	            <?php if ( $fee_label ) { ?>
                    <table class="delivrery-fee" id="delivrery-fee">
                        <tbody>
                        <tr>
                            <td><?= $fee_label ?></td>
                        </tr>
                        </tbody>
                    </table>
	            <?php } ?>
            </div>
        </div>
	<?php else : ?>
        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordered" id="infos">
                    <thead>
                    <tr>
                        <th class="active">Declaración conjunta</th>
                        <th class="active">Niños menores de 3 años</th>
                        <th class="active">Niños entre 3 y 25 años</th>
                        <th class="active">Salario bruto</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?php if ( ! empty( $declarationcommune ) ) {
								echo $declarationcommune;
							} ?></td>
                        <td><?php if ( ! empty( $nbrchild ) ) {
								echo $nbrchild;
							} ?></td>
                        <td><?php if ( ! empty( $nbrchildunderthree ) ) {
								echo $nbrchildunderthree;
							} ?></td>
                        <td><?php if ( ! empty( $salairebrut ) ) {
								echo number_format( $salairebrut, 2, ',', ' ' );
							} ?> €
                        </td>

                    </tr>
                    </tbody>
                </table>
				<?php if ( $foodpassselected == 'true' ): ?>
                    <table class="table table-bordered" id="infos">
                        <thead>
                        <tr>
                            <th class="active"><h4><b>Restaurante Pass</b></h4></th>
                        </tr>
                        </thead>
                        <thead>
                        <tr>
                            <th class="active">Importe diario (€)</th>
                            <th class="active">Número de meses</th>
                            <th class="active">Total (€)</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php if ( ! empty( $foodpassimporte ) ) {
									echo number_format( $foodpassimporte, 2, ',', ' ' );
								} ?></td>
                            <td><?php if ( ! empty( $foodpassmonth ) ) {
									echo $foodpassmonth;
								} ?></td>
                            <td><?php if ( ! empty( $foodpasstotal ) ) {
									echo number_format( $foodpasstotal, 2, ',', ' ' );
								} ?> €
                            </td>
                        </tr>
                        </tbody>
                    </table>
				<?php endif ?>
				<?php if ( $transportpassselected == 'true' ): ?>
                    <table class="table table-bordered" id="infos">
                        <thead>
                        <tr>
                            <th class="active"><h4><b>Transporte Pass</b></h4></th>
                        </tr>
                        </thead>
                        <thead>
                        <tr>
                            <th class="active">Importe anual (€)</th>
                            <th class="active">Número de meses</th>
                            <th class="active">Total</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php if ( ! empty( $transportpassimporte ) ) {
									echo $transportpassimporte;
								} ?></td>
                            <td><?php if ( ! empty( $transportpassmonth ) ) {
									echo $transportpassmonth;
								} ?></td>
                            <td><?php if ( ! empty( $transportpasstotal ) ) {
									echo number_format( $transportpasstotal, 2, ',', ' ' );
								} ?> €
                            </td>
                        </tr>
                        </tbody>
                    </table>
				<?php endif ?>
				<?php if ( $childcarepassselected == 'true' ): ?>
                    <table class="table table-bordered" id="infos">
                        <thead>
                        <tr>
                            <th class="active"><h4><b>Guardería Pass</b></h4></th>
                        </tr>
                        </thead>
                        <thead>
                        <tr>
                            <th class="active">Importe mensual (€)</th>
                            <th class="active">Número de meses</th>
                            <th class="active">Total</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php if ( ! empty( $childcarepassimporte ) ) {
									echo number_format( $childcarepassimporte, 2, ',', ' ' );
								} ?></td>
                            <td><?php if ( ! empty( $childcarepassmonth ) ) {
									echo $childcarepassmonth;
								} ?></td>
                            <td><?php if ( ! empty( $childcarepasstotal ) ) {
									echo number_format( $childcarepasstotal, 2, ',', ' ' );
								} ?> €
                            </td>
                        </tr>
                        </tbody>
                    </table>
				<?php endif ?>
				<?php if ( $healthpassselected == 'true' ): ?>
                    <table class="table table-bordered" id="infos">
                        <thead>
                        <tr>
                            <th class="active"><h4><b>Seguro de salud</b></h4></th>
                        </tr>
                        </thead>
                        <thead>
                        <tr>
                            <th class="active">Importe anual (€)</th>
                            <th class="active">Número de asegurados</th>
                            <th class="active">Total</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php if ( ! empty( $healthpassimporte ) ) {
									echo number_format( $healthpassimporte, 2, ',', ' ' );
								} ?></td>
                            <td><?php if ( ! empty( $healthpassmonth ) ) {
									echo $healthpassmonth;
								} ?></td>
                            <td><?php if ( ! empty( $healthpasstotal ) ) {
									echo number_format( $healthpasstotal, 2, ',', ' ' );
								} ?></td>
                        </tr>
                        </tbody>
                    </table>
				<?php endif ?>
				<?php if ( $trainingpassselected == 'true' ): ?>
                    <table class="table table-bordered" id="infos">
                        <thead>
                        <tr>
                            <th class="active"><h4><b>Formación Pass</b></h4></th>
                        </tr>
                        </thead>
                        <thead>
                        <tr>
                            <th class="active">Importe anual (€)</th>
                            <th class="active">Total</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php if ( ! empty( $trainingpassimporte ) ) {
									echo $trainingpassimporte;
								} ?></td>
                            <td><?php if ( ! empty( $trainingpassmonth ) ) {
									echo number_format( $trainingpassmonth, 2, ',', ' ' );
								} ?></td>
                        </tr>
                        </tbody>
                    </table>
				<?php endif ?>
                <table class="table table-bordered" id="result">
                    <tbody>
                    <tr>
                        <td class="info"><b>Ahorro: </b></td>
                        <td class="info"><b>IRPF sin Retribución Flexible (€)</b></td>
                        <td width="10%"><?php if ( ! empty( $totaltaxwithoutsalarysacrifice ) ) {
								echo number_format( $totaltaxwithoutsalarysacrifice, 2, ',', '&nbsp;' );
							} ?></td>
                        <td class="info"><b>IRPF con Retribución Flexible (€)</b></td>
                        <td width="10%"><?php if ( ! empty( $totaltaxwithsalarysacrifice ) ) {
								echo number_format( $totaltaxwithsalarysacrifice, 2, ',', '&nbsp;' );
							} ?></td>
                        <td class="info"><b>Importe destinado a Retribución Flexible (€)</b></td>
                        <td width="10%"><?php if ( ! empty( $totalfacialbenefits ) ) {
								echo number_format( $totalfacialbenefits, 2, ',', '&nbsp;' );
							} ?></td>
                    </tr>
                    </tbody>
                </table>
                <table class="table table-bordered" id="economy">
                    <thead>
                    <th class="active">Tu ahorro de IRPF es</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?php if ( ! empty( $eurosave ) ) {
								echo number_format( $eurosave, 2, ',', ' ' );
							} ?> €
                        </td>
                    </tr>
                    </tbody>
                </table>
	            <?php if ( $fee_label ) { ?>
                    <table class="delivrery-fee" id="delivrery-fee">
                        <tbody>
                        <tr>
                            <td><?= $fee_label ?></td>
                        </tr>
                        </tbody>
                    </table>
	            <?php } ?>
            </div>
        </div>
	<?php endif ?>
	<?php if ( $link ): ?>
        <div class="row" id="advantage"><?php echo $link['url'] ?></div>
	<?php else: ?>
        <div class="row" id="advantage">Sodexo advantages</div>
	<?php endif ?>
</div>
</body>
</html>
