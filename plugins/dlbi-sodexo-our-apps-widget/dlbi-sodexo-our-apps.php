<?php

/*
  Plugin Name: Sodexo Widget Our Apps
  Description: Sodexo Widget Our Apps
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-sodexo-our-apps-widget
 */


// Plugin Consts
define('SOD_OUR_VERSION', '1.0');
define('SOD_OUR_URL', plugins_url('', __FILE__));
define('SOD_OUR_DIR', dirname(__FILE__));

//Load class
require( SOD_OUR_DIR . '/inc/class-our-apps-widget.php' );

//Init widget our apps
function dlbi_sodexo_our_apps_init() {
    register_widget('SodexoWidget_Our_Apps');

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-sodexo-our-apps-widget', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('widgets_init', 'dlbi_sodexo_our_apps_init');
