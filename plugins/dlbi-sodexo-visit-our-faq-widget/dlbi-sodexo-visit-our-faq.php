<?php

/*
  Plugin Name: Sodexo Widget Visit Our FAQ
  Description: Sodexo Widget Visit Our FAQ
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-sodexo-visit-our-faq-widget
 */


// Plugin Consts
define('SOD_VIS_VERSION', '1.0');
define('SOD_VIS_URL', plugins_url('', __FILE__));
define('SOD_VIS_DIR', dirname(__FILE__));

//Load class
require( SOD_VIS_DIR . '/inc/class-visit-our-faq-widget.php' );

//Init widget our apps
function dlbi_sodexo_visit_our_faq_init() {
    register_widget('SodexoWidget_Visit_Our_Faq');

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-sodexo-visit-our-faq-widget', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('widgets_init', 'dlbi_sodexo_visit_our_faq_init');
