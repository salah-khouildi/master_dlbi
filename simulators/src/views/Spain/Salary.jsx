import React from 'react'
// import PropTypes from 'prop-types'
import update from 'immutability-helper'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import './salary.scss'

import Counter from './../../components/Counter/Counter'
import Toggle from './../../components/Toggle/Toggle'
import SliderTooltip from './../../components/SliderTooltip/SliderTooltip'
import Doughnut from './../../components/Doughnut/Doughnut'
// import DoughnutSummary from './../../components/Doughnut/DoughnutSummary'
import Selector from './../../components/Selector/Selector'
import Messages from './../../components/Messages/Messages'

import SacrificeSimulator from './../../utils/spainSalarySimulator.js'

import axios from 'axios'
// import classNames from 'classnames'

class SpainSalaryApp extends React.Component {
  constructor (props) {
    // Pass props to parent class
    super(props)

    this.declarationCommuneChange = this.declarationCommuneChange.bind(this)
    this.salaireBrutChange = this.salaireBrutChange.bind(this)
    this.nbrChildUnderThreeChange = this.nbrChildUnderThreeChange.bind(this)
    this.nbrChildUnderTwentyfiveChange = this.nbrChildUnderTwentyfiveChange.bind(this)
    this.nbrChildOverTwentyfiveChange = this.nbrChildOverTwentyfiveChange.bind(this)
    this.productsSelectedChange = this.productsSelectedChange.bind(this)

    this.productsSelectedChange = this.productsSelectedChange.bind(this)

    this.handleClickShowMore = this.handleClickShowMore.bind(this)

    this.onClickTypeform = this.onClickTypeform.bind(this)
    this.onClickSendEmail = this.onClickSendEmail.bind(this)
    this.onClickDownloadPdf = this.onClickDownloadPdf.bind(this)
    this.closeTypeform = this.closeTypeform.bind(this)
    this.createIframe = this.createIframe.bind(this)

    window.dataLayer = window.dataLayer || []

    this._getDataParams()

        // Set initial state
    this.state = {
      // Data inputs
      dataInput: {
        salaireBrut: 25000,
        nbrChildUnderThree: 0,
        nbrChildUnderTwentyfive: 0,
        nbrChildOverTwentyfive: 0,
        declarationCommune: false,
        valeurTicketResto: 0,
        nombreMoisResto: 0,
        dedomagementTransport: 0,
        nombreMoisTransport: 0,
        montantGarderieMensuel: 0,
        nombreMoisGarderie: 0,
        budgetAnnuelSante: 0,
        nombreAssures: 0,
        montantAnnuelFormation: 0,
        nbrChild: 0,
      },
      // Results
      dataOutput: {
        socialSecurityTaxes: 0,
        totalFacialBenefits: 0,
        totalAnnualTaxWithoutSalarySacrifice: 0,
        totalAnnualTaxWithSalarySacrifice: 0,
        netSalaryAvailableWithoutSalarySacrifice: 0,
        netSalaryAvailableWithSalarySacrifice: 0,
        annualEuroSave: 0,
        percent: 0,

      },
      dataProducts: {
        foodPass: {
          type: 'foodPass',
          selected: true,
          title: 'Restaurante Pass',
          total: 0,
          firstValue : {
            value: 9,
            title: 'Importe diario (€)',
            min: 1,
            max: 9,
            step: 1,
            stepInput: 1
          },
          secondValue: {
            value: 11,
            title: 'Número de meses',
            min: 1,
            max: 11,
            step: 1,
            stepInput: 1
          },
        },
        childcarePass: {
          type: 'childcarePass',
          selected: false,
          title: 'Guardería Pass',
          total: 0,
          firstValue : {
            value: 550,
            title: 'Importe mensual (€)',
            min: 0,
            max: 999,
            step: 1,
            stepInput: 1
          },
          secondValue: {
            value: 1,
            title: 'Número de meses',
            min: 0,
            max: 11,
            step: 1,
            stepInput: 1
          },
        },
        transportPass: {
          type: 'transportPass',
          selected: false,
          title: 'Transporte Pass',
          total: 0,
          firstValue : {
            value: 10,
            title: 'Importe mensual (€)',
            min: 10,
            max: 136.36,
            step: 1,
            stepInput: 0.01
          },
          secondValue: {
            value: 1,
            title: 'Número de meses',
            min: 1,
            max: 11,
            step: 1,
            stepInput: 1
          },
        },
        healthPass: {
          type: 'healthPass',
          selected: false,
          title: 'Seguro de salud',
          total: 0,
          firstValue : {
            value: 0,
            title: 'Importe anual (€)',
            min: 0,
            max: 500,
            step: 1,
            stepInput: 1
          },
          secondValue: {
            value: 1,
            title: 'Número de asegurados',
            min: 1,
            max: 5,
            step: 1,
            stepInput: 1
          },
        },
        trainingPass: {
          type: 'trainingPass',
          selected: false,
          title: 'Formación Pass',
          total: 0,
          firstValue : {
            value: 0,
            title: 'Importe anual (€)',
            min: 0,
            max: 20000,
            step: 1,
            stepInput: 1
          },
          secondValue: {
            value: 1,
            title: '',
            min: 1,
            max: 1,
            step: 1,
            stepInput: 1
          },
        }
      },
      messages: [],
      showMore: false,
      showTypeform: false,
      loading: true,
      displayMessageError: false,
      displayMessageAdvice: true,
      messageAdvice: '',
      messageError: '',
      showCounter: false,
      nbrChild: 0,
      percentage: 80
    }
  }

 /* UTILS */

  _getDataParams () {
    let url = document.location.origin + '/wp-json/acf/v2/options/'
    // let url = document.location.origin + '/front-wp-dev/wp-json/acf/v2/options/'
    if (__DEV__) {
      url = 'http://10.224.86.238:3000/wp-json/acf/v2/options/'
      // url = 'http:/sodexo.dev/wp-json/acf/v2/options/'
      // url = 'http://10.0.75.1:3000/front-wp-dev/wp-json/acf/v2/options/'
    }

    return axios({
      method:'get',
      url: url,
    })
    .then((response) => {
      if (response.status === 200) {
        return this.setState({ dataParams: response.data.acf }, () => {
          this.state.dataProducts.foodPass.firstValue.max = response.data.acf['max_food_per_day']
    	    this.state.dataProducts.transportPass.firstValue.max = response.data.acf['max_transport_per_year'] / 12
	        this.state.dataProducts.childcarePass.firstValue.max = response.data.acf['max_childcare']
	        this.simulator = new SacrificeSimulator(
            this.state
          )

          const newDataOutput = update(this.state.dataOutput, { $merge: {
            socialSecurityTaxes: this.simulator.dataOutput.socialSecurityTaxes,
            totalFacialBenefits: this.simulator.dataOutput.totalFacialBenefits,
            totalAnnualTaxWithoutSalarySacrifice: this.simulator.dataOutput.totalAnnualTaxWithoutSalarySacrifice,
            totalAnnualTaxWithSalarySacrifice: this.simulator.dataOutput.totalAnnualTaxWithSalarySacrifice,
            netSalaryAvailableWithoutSalarySacrifice: this.simulator.dataOutput.netSalaryAvailableWithoutSalarySacrifice,
            netSalaryAvailableWithSalarySacrifice: this.simulator.dataOutput.netSalaryAvailableWithSalarySacrifice,
            annualEuroSave: this.simulator.dataOutput.annualEuroSave
          } })

          this.setState({
            typeform: this.state.dataParams['get_a_quote_salary_sacrifice'],
            ctaSendPerEmailLabel:  this.state.dataParams['pdf_modal_title'],
            ctaBeContactedLabel:  this.state.dataParams['simulator_be_contacted_label'],
            ctaDownloadLabel:  this.state.dataParams['simulator_download_label'],
            ctaDisclaimer:  this.state.dataParams['simulator_legend'],
            ctaPLaceanOrderLabel:  this.state.dataParams['simulator_place_an_order_label'],
            ctaPLaceanOrderLink:  this.state.dataParams['simulator_place_an_order_link'],
            dataOutput: newDataOutput,
            totalAmount: this.state.dataParams['total_amount'],
            loading: false
          })
        })
      }
    })
  }

  _updateSimultorState (value, state) {
    window.dataLayer.push({ 'event':'click-simulator' })
    const newDataInput = update(this.state.dataInput, { $merge: value })
    this.setState({ dataInput: newDataInput }, () => {
      this.simulator = new SacrificeSimulator(this.state)
      const newDataOutput = update(this.state.dataOutput, { $merge: {
        socialSecurityTaxes: this.simulator.dataOutput.socialSecurityTaxes,
        totalFacialBenefits: this.simulator.dataOutput.totalFacialBenefits,
        totalAnnualTaxWithoutSalarySacrifice: this.simulator.dataOutput.totalAnnualTaxWithoutSalarySacrifice,
        totalAnnualTaxWithSalarySacrifice: this.simulator.dataOutput.totalAnnualTaxWithSalarySacrifice,
        netSalaryAvailableWithoutSalarySacrifice: this.simulator.dataOutput.netSalaryAvailableWithoutSalarySacrifice,
        netSalaryAvailableWithSalarySacrifice: this.simulator.dataOutput.netSalaryAvailableWithSalarySacrifice,
        annualEuroSave: this.simulator.dataOutput.annualEuroSave
      } })

      if (this.simulator.dataOutput.annualEuroSave === 0) {
        this.setState({ percentage: 0 })
      } else {
        this.setState({ percentage: 80 })
      }

      if (((newDataOutput.totalAnnualTaxWithSalarySacrifice * 100) / newDataOutput.totalAnnualTaxWithoutSalarySacrifice) === 0 || isNaN((newDataOutput.totalAnnualTaxWithSalarySacrifice * 100) / this.state.dataOutput.totalAnnualTaxWithoutSalarySacrifice)) {
        this.setState({ displayMessageAdvice: true }, () => {
          this.setState({ messageAdvice: this.getMessage(this.state.dataParams.simulator_message[1]) })
        })
      } else {
        document.getElementById('results').classList.remove('overlay')
        this.setState({ displayMessageAdvice: false }, () => {
          this.setState({ messageAdvice: '' })
        })
      }

      if (newDataOutput.totalFacialBenefits > (this.state.totalAmount * this.state.dataInput.salaireBrut)) {
        this.setState({ displayMessageError: true }, () => {
          this.setState({ messageError: this.getMessage(this.state.dataParams.simulator_message[0]) })
          this.setState({ messageAdvice: '' })
        })
      } else {
        document.getElementById('results').classList.remove('overlay')
        let element = document.getElementsByClassName('product')
        for (let i = 0; i < element.length; i++) {
          element[i].classList.remove('product-error')
        }
        this.setState({ displayMessageError: false }, () => {
          this.setState({ messageError: '' })
        })
      }

      this.setState({ dataOutput: newDataOutput })
    })
  }

  _toQueryString (obj) {
    let parts = []
    for (var i in obj) {
      if (obj.hasOwnProperty(i)) {
        parts.push(encodeURIComponent(i) + '=' + encodeURIComponent(obj[i]))
      }
    }
    return parts.join('&')
  }

  /* ON CHANGE */

  declarationCommuneChange (value) {
    return this._updateSimultorState({ declarationCommune: value }, this.state)
  }

  nbrChildChange (value) {
    this.setState({ nbrChild : value }, () => {
      if (value > 0) {
        this.setState({ showCounter : true })
      } else {
        this.setState({ showCounter : false })
      }
    })
    return this._updateSimultorState({ nbrChild: value }, this.state)
  }

  nbrChildUnderThreeChange (value) {
    return this._updateSimultorState({ nbrChildUnderThree: value }, this.state)
  }

  nbrChildUnderTwentyfiveChange (value) {
    return this._updateSimultorState({ nbrChildUnderTwentyfive: value }, this.state)
  }

  nbrChildOverTwentyfiveChange (value) {
    return this._updateSimultorState({ nbrChildOverTwentyfive: value }, this.state)
  }

  salaireBrutChange (value) {
    return this._updateSimultorState({ salaireBrut: value }, this.state)
  }

  productsSelectedChange (value) {
    this.setState({ dataProducts: value }, () => {
      this.simulator = new SacrificeSimulator(this.state)
      const newDataOutput = update(this.state.dataOutput, { $merge: {
        socialSecurityTaxes: this.simulator.dataOutput.socialSecurityTaxes,
        totalFacialBenefits: this.simulator.dataOutput.totalFacialBenefits,
        totalAnnualTaxWithoutSalarySacrifice: this.simulator.dataOutput.totalAnnualTaxWithoutSalarySacrifice,
        totalAnnualTaxWithSalarySacrifice: this.simulator.dataOutput.totalAnnualTaxWithSalarySacrifice,
        netSalaryAvailableWithoutSalarySacrifice: this.simulator.dataOutput.netSalaryAvailableWithoutSalarySacrifice,
        netSalaryAvailableWithSalarySacrifice: this.simulator.dataOutput.netSalaryAvailableWithSalarySacrifice,
        annualEuroSave: this.simulator.dataOutput.annualEuroSave,
      } })

      if (this.simulator.dataOutput.annualEuroSave === 0) {
        this.setState({ percentage: 0 })
      } else {
        this.setState({ percentage: 80 })
      }

      if (((newDataOutput.totalAnnualTaxWithSalarySacrifice * 100) / newDataOutput.totalAnnualTaxWithoutSalarySacrifice) === 0 || isNaN((newDataOutput.totalAnnualTaxWithSalarySacrifice * 100) / this.state.dataOutput.totalAnnualTaxWithoutSalarySacrifice)) {
        this.setState({ displayMessageAdvice: true }, () => {
          this.setState({ messageAdvice: this.getMessage(this.state.dataParams.simulator_message[1]) })
        })
      } else {
        document.getElementById('results').classList.remove('overlay')
        this.setState({ displayMessageAdvice: false }, () => {
          this.setState({ messageAdvice: '' })
        })
      }

      if (newDataOutput.totalFacialBenefits > (this.state.totalAmount * this.state.dataInput.salaireBrut)) {
        this.setState({ displayMessageError: true }, () => {
          this.setState({ messageError: this.getMessage(this.state.dataParams.simulator_message[0]) })
          this.setState({ messageAdvice: '' })
        })
      } else {
        document.getElementById('results').classList.remove('overlay')
        let element = document.getElementsByClassName('product')
        for (let i = 0; i < element.length; i++) {
          element[i].classList.remove('product-error')
        }
        this.setState({ displayMessageError: false }, () => {
          this.setState({ messageError: '' })
        })
      }

      this.setState({ dataOutput: newDataOutput })
    })
  }

  handleClickShowMore () {
    this.setState({ showMore: !this.state.showMore }, () => {
      // console.log(this.state.showMore)
      // if (this.state.showMore) {
      //   document.getElementById('root').classList.add('overlay')
      // } else {
      //   document.getElementById('root').classList.remove('overlay')
      // }
    })
  }

  getMessage (message) {
    return (
      <Messages
        type={message.simulator_type_of_message}
        title={message.simulator_title}
        message={message.simulator_message}
      />
    )
  }

  /* CTA PART */

  onClickSendEmail (event) {
    event.preventDefault()
    let data = {
      simulator: 'salary',
      declarationcommune: this.state.dataInput.declarationCommune,
      nbrchild: this.state.dataInput.nbrChild,
      nbrchildunderthree: this.state.dataInput.nbrChildUnderThree,
      salairebrut: this.state.dataInput.salaireBrut,

      foodpassselected: this.state.dataProducts.foodPass.selected,
      foodpassimporte: this.state.dataProducts.foodPass.selected.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' }) ? this.state.dataProducts.foodPass.firstValue.value : '0'.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' }),
      foodpassmonth: this.state.dataProducts.foodPass.selected ? this.state.dataProducts.foodPass.secondValue.value : 0,
      foodpasstotal: this.state.dataProducts.foodPass.selected.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' }) ? this.state.dataProducts.foodPass.total : '0'.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' }),

      transportpassselected: this.state.dataProducts.transportPass.selected,
      transportpassimporte: this.state.dataProducts.transportPass.selected ? this.state.dataProducts.transportPass.firstValue.value : 0,
      transportpassmonth: this.state.dataProducts.transportPass.selected ? this.state.dataProducts.transportPass.secondValue.value : 0,
      transportpasstotal: this.state.dataProducts.transportPass.selected ? this.state.dataProducts.transportPass.total : 0,

      childcarepassselected: this.state.dataProducts.childcarePass.selected,
      childcarepassimporte: this.state.dataProducts.childcarePass.selected ? this.state.dataProducts.childcarePass.firstValue.value : 0,
      childcarepassmonth: this.state.dataProducts.childcarePass.selected ? this.state.dataProducts.childcarePass.secondValue.value : 0,
      childcarepasstotal: this.state.dataProducts.childcarePass.selected ? this.state.dataProducts.childcarePass.total : 0,

      healthpassselected: this.state.dataProducts.healthPass.selected,
      healthpassimporte: this.state.dataProducts.healthPass.selected ? this.state.dataProducts.healthPass.firstValue.value : 0,
      healthpassmonth: this.state.dataProducts.healthPass.selected ? this.state.dataProducts.healthPass.secondValue.value : 0,
      healthpasstotal: this.state.dataProducts.healthPass.selected ? this.state.dataProducts.healthPass.total : 0,

      trainingpassselected: this.state.dataProducts.trainingPass.selected,
      trainingpassimporte: this.state.dataProducts.trainingPass.selected ? this.state.dataProducts.trainingPass.firstValue.value : 0,
      trainingpasstotal: this.state.dataProducts.trainingPass.selected ? this.state.dataProducts.trainingPass.total : 0,

      totaltaxwithoutsalarysacrifice: this.state.dataOutput.totalAnnualTaxWithoutSalarySacrifice,
      totaltaxwithsalarysacrifice: this.state.dataOutput.totalAnnualTaxWithSalarySacrifice,
      totalfacialbenefits: this.state.dataOutput.totalFacialBenefits,
      eurosave: this.state.dataOutput.annualEuroSave
    }

    data = this._toQueryString(data)
    document.getElementById('mail-pdf-form').action = document.location.origin + this.state.dataParams['pdf_action_url_adress'] + '?' + data
  }

  onClickDownloadPdf (event) {
    event.preventDefault()
    window.dataLayer.push({ 'event': 'download', 'name':'PDF simulador España Retribución Flexible' })
    let data = {
      simulator: 'salary',
      declarationcommune: this.state.dataInput.declarationCommune,
      nbrchild: this.state.dataInput.nbrChild,
      nbrchildunderthree: this.state.dataInput.nbrChildUnderThree,
      salairebrut: this.state.dataInput.salaireBrut,

      foodpassselected: this.state.dataProducts.foodPass.selected,
      foodpassimporte: this.state.dataProducts.foodPass.selected ? this.state.dataProducts.foodPass.firstValue.value : 0,
      foodpassmonth: this.state.dataProducts.foodPass.selected ? this.state.dataProducts.foodPass.secondValue.value : 0,
      foodpasstotal: this.state.dataProducts.foodPass.selected ? this.state.dataProducts.foodPass.total : 0,

      transportpassselected: this.state.dataProducts.transportPass.selected,
      transportpassimporte: this.state.dataProducts.transportPass.selected ? this.state.dataProducts.transportPass.firstValue.value : 0,
      transportpassmonth: this.state.dataProducts.transportPass.selected ? this.state.dataProducts.transportPass.secondValue.value : 0,
      transportpasstotal: this.state.dataProducts.transportPass.selected ? this.state.dataProducts.transportPass.total : 0,

      childcarepassselected: this.state.dataProducts.childcarePass.selected,
      childcarepassimporte: this.state.dataProducts.childcarePass.selected ? this.state.dataProducts.childcarePass.firstValue.value : 0,
      childcarepassmonth: this.state.dataProducts.childcarePass.selected ? this.state.dataProducts.childcarePass.secondValue.value : 0,
      childcarepasstotal: this.state.dataProducts.childcarePass.selected ? this.state.dataProducts.childcarePass.total : 0,

      healthpassselected: this.state.dataProducts.healthPass.selected,
      healthpassimporte: this.state.dataProducts.healthPass.selected ? this.state.dataProducts.healthPass.firstValue.value : 0,
      healthpassmonth: this.state.dataProducts.healthPass.selected ? this.state.dataProducts.healthPass.secondValue.value : 0,
      healthpasstotal: this.state.dataProducts.healthPass.selected ? this.state.dataProducts.healthPass.total : 0,

      trainingpassselected: this.state.dataProducts.trainingPass.selected,
      trainingpassimporte: this.state.dataProducts.trainingPass.selected ? this.state.dataProducts.trainingPass.firstValue.value : 0,
      trainingpasstotal: this.state.dataProducts.trainingPass.selected ? this.state.dataProducts.trainingPass.total : 0,

      totaltaxwithoutsalarysacrifice: this.state.dataOutput.totalAnnualTaxWithoutSalarySacrifice,
      totaltaxwithsalarysacrifice: this.state.dataOutput.totalAnnualTaxWithSalarySacrifice,
      totalfacialbenefits: this.state.dataOutput.totalFacialBenefits,
      eurosave: this.state.dataOutput.annualEuroSave
    }

    data = this._toQueryString(data)
    window.location.href = document.location.origin + this.state.dataParams['pdf_action_url_adress'] + '?' + data
  }


    onClickTypeform (event) {
        event.preventDefault()
        document.body.classList.add("no-scroll");
        document.body.classList.add("overlay");
        this.setState({ showTypeform: true })
        window.dataLayer.push({ 'event':'click-contactus', 'location':'simulator' })
    }

    closeTypeform (event) {
        event.preventDefault()
        document.body.classList.remove("no-scroll");
        document.body.classList.remove("overlay");
        this.setState({ showTypeform: false })
    }

  createIframe () {
    let data = {
      declarationcommune: this.state.dataInput.declarationCommune,
      nbrchild: this.state.dataInput.nbrChild,
      nbrchildunderthree: this.state.dataInput.nbrChildUnderThree,
      salairebrut: this.state.dataInput.salaireBrut,

      // foodpassselected: this.state.dataProducts.foodPass.selected,
      foodpassimporte: this.state.dataProducts.foodPass.selected ? this.state.dataProducts.foodPass.firstValue.value : 0,
      foodpassmonth: this.state.dataProducts.foodPass.selected ? this.state.dataProducts.foodPass.secondValue.value : 0,
      foodpasstotal: this.state.dataProducts.foodPass.selected ? this.state.dataProducts.foodPass.total : 0,

      // transportpassselected: this.state.dataProducts.transportPass.selected,
      transportpassimporte: this.state.dataProducts.transportPass.selected ? this.state.dataProducts.transportPass.firstValue.value : 0,
      transportpassmonth: this.state.dataProducts.transportPass.selected ? this.state.dataProducts.transportPass.secondValue.value : 0,
      transportpasstotal: this.state.dataProducts.transportPass.selected ? this.state.dataProducts.transportPass.total : 0,

      // childcarepassselected: this.state.dataProducts.childcarePass.selected,
      childcarepassimporte: this.state.dataProducts.childcarePass.selected ? this.state.dataProducts.childcarePass.firstValue.value : 0,
      childcarepassmonth: this.state.dataProducts.childcarePass.selected ? this.state.dataProducts.childcarePass.secondValue.value : 0,
      childcarepasstotal: this.state.dataProducts.childcarePass.selected ? this.state.dataProducts.childcarePass.total : 0,

      // healthpassselected: this.state.dataProducts.healthPass.selected,
      healthpassimporte: this.state.dataProducts.healthPass.selected ? this.state.dataProducts.healthPass.firstValue.value : 0,
      healthpassmonth: this.state.dataProducts.healthPass.selected ? this.state.dataProducts.healthPass.secondValue.value : 0,
      healthpasstotal: this.state.dataProducts.healthPass.selected ? this.state.dataProducts.healthPass.total : 0,

      // trainingpassselected: this.state.dataProducts.trainingPass.selected,
      trainingpassimporte: this.state.dataProducts.trainingPass.selected ? this.state.dataProducts.trainingPass.firstValue.value : 0,
      trainingpasstotal: this.state.dataProducts.trainingPass.selected ? this.state.dataProducts.trainingPass.total : 0,

      totaltaxwithoutsalarysacrifice: this.state.dataOutput.totalAnnualTaxWithoutSalarySacrifice,
      totaltaxwithsalarysacrifice: this.state.dataOutput.totalAnnualTaxWithSalarySacrifice,
      totalfacialbenefits: this.state.dataOutput.totalFacialBenefits,
      eurosave: this.state.dataOutput.annualEuroSave
    }

    data = this._toQueryString(data)
    return {
      __html: '<div class="iframe-wrapper"><iframe src="' + this.state.typeform + '?' + data + '" width="540" height="450"></iframe></div>'
    }
  }

  /* RESULTS RENDER */

  getFullResults (className, width, height) {
    return (
      <div id='box' className={className} key='key'>
        <h3 className='results-title simulator-title'>Ahorro:</h3>

        <ul className='results-list'>
          <li>
            <p className='results-label'>IRPF sin Retribución Flexible</p>
            <p className='results-value'>{ this.state.dataOutput.totalAnnualTaxWithoutSalarySacrifice.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' })}</p>
          </li>
          <li>
            <p className='results-label'>IRPF con Retribución Flexible</p>
            <p className='results-value'>{ this.state.dataOutput.totalAnnualTaxWithSalarySacrifice.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' })}</p>
          </li>
          <li>
            <p className='results-label'>Importe destinado a Retribución Flexible</p>
            <p className='results-value'>{ this.state.dataOutput.totalFacialBenefits.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' })}</p>
          </li>
        </ul>

        <Doughnut
          className={className + '_doughnut'}
          width={width}
          height={height}
          percent={this.state.percentage}
          duration={1500}
          customTextTop={'Tu ahorro de IRPF es'}
          customTextBottom={this.state.dataOutput.annualEuroSave.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' })}
        />
      </div>
    )
  }

  /* RENDER */

  render () {
    let showMoreClass = this.state.showMore ? 'open' : ''
    let showCounter = this.state.showCounter ? 'show-counter' : ''
    // let showMoreText = this.state.showMore ? '-' : '+'
    let showMoreCheuvronClass = this.state.showMore ? '' : 'bottom'
    let component = this.state.showMore ? this.getFullResults('results__mobile_full', 180, 180) : ''
    let showTypeform = this.state.showTypeform ? 'show-typeform' : ''
    // let showTypeformOverlay = this.state.showTypeform ? 'overlay' : ''
    let nbrchildMax = this.state.nbrChild

    if (this.state.loading) {
      return (
        <div className='loading'>
          <p>Loading...</p>
        </div>
      )
    } else {
      return (
        <div>
          <section className={'simulator spain '}>
            <div className='settings'>
              <div className='container_simulator' id='scrollbar'>
                <div className='settings__aboutYou'>
                  <h3 className='settings__aboutYou--title simulator-title'>Tus datos:</h3>

                  <div className='settings__aboutYou--block'>
                    <div>
                      <p className='simulator-label'>Declaración conjunta</p>
                      <Toggle
                        trueLabel={'Sí'}
                        falseLabel={'No'}
                        defaultChecked={!this.state.declarationCommune}
                        onChange={(value) => { this.declarationCommuneChange(value) }}
                      />
                    </div>

                    {/* <div>
                      <p className='simulator-label'>Número de hijos</p>
                      <Counter
                        value={this.state.dataInput.nbrChildUnderTwentyfive}
                        min={0}
                        max={10}
                        onChange={(value) => { this.nbrChildUnderTwentyfiveChange(value) }}
                      />
                    </div> */}

                    <div>
                      <p className='simulator-label'>Número de hijos</p>
                      <Counter
                        value={this.state.dataInput.nbrChild}
                        min={0}
                        max={10}
                        onChange={(value) => { this.nbrChildChange(value) }}
                      />
                    </div>

                    <div className={'counter-hidden ' + showCounter}>
                      <p className='simulator-label'>Menores de 3 años</p>
                      <Counter
                        value={this.state.dataInput.nbrChildUnderThree}
                        min={0}
                        max={nbrchildMax}
                        onChange={(value) => { this.nbrChildUnderThreeChange(value) }}
                      />
                    </div>

                    {/* <div >
                      <p className='simulator-label'>Niños mayores de 25 años</p>
                      <Counter
                        value={this.state.dataInput.nbrChildOverTwentyfive}
                        min={0}
                        max={10}

                        onChange={(value) => { this.nbrChildOverTwentyfiveChange(value) }}
                      />
                    </div> */}
                  </div>

                  <div className='settings__aboutYou--block'>
                    <p className='simulator-label'>Salario bruto</p>
                    <SliderTooltip
                      value={this.state.dataInput.salaireBrut}
                      min={15000}
                      max={100000}
                      step={5000}
                      placement='top'
                      onChange={(value) => { this.salaireBrutChange(value) }}
                    />
                  </div>
                </div>

                <div className='settings__products'>
                  <Selector
                    products={this.state.dataProducts}
                    onChange={(value) => { this.productsSelectedChange(value) }}
                  />
                </div>
              </div>
            </div>

            <div id='results' className='results'>
              <div className={'results__mobile ' + showMoreClass}>
                <div className='results__mobile_summary'>
                  {/* <DoughnutSummary
                    className={'results__mobile_summary_doughnut'}
                    width={50}
                    height={50}
                    percent={this.state.percentage}
                    duration={1200}
                  /> */}

                  <div className='results__mobile_summary_content'>
                    <span className='results__mobile_summary_content-text'>Tu ahorro de IRPF es</span>
                    <span className='results__mobile_summary_content-percentage'>{ this.state.dataOutput.annualEuroSave.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' }) }</span>
                  </div>
                </div>

                <ReactCSSTransitionGroup
                  transitionName='slide'
                  transitionAppear
                  transitionAppearTimeout={500}
                  transitionEnterTimeout={300}
                  transitionLeaveTimeout={300}
                >
                  {component}
                </ReactCSSTransitionGroup>
                <div className={'results__show-more ' + showMoreClass} onClick={this.handleClickShowMore}>
                  <span className={'chevron ' + showMoreCheuvronClass} />
                </div>
              </div>

              { this.getFullResults('results__full', 250, 250) }

              <div className='results__messages'>
                { this.state.messageAdvice }
                { this.state.messageError }
              </div>

            </div>

              <div className={'iframe-typeform-wrapper ' + showTypeform}>
                  <div className='iframe-typeform'>
                      <div dangerouslySetInnerHTML={this.createIframe()} />
              <a className='product-close' onClick={this.closeTypeform} href='#'><i className='icon-close'></i></a>
            </div>
            </div>
          </section>

          <section className='callToAction'>
            <div className='callToAction-links'>
              <div className='callToAction-groupLinks'>
                <a href='#' onClick={this.onClickDownloadPdf} className='callToAction-link'>{this.state.ctaDownloadLabel}</a>
                <a href='#' className='callToAction-link' onClick={this.onClickSendEmail} data-toggle='modal' data-target='#sendmailmodal'>{this.state.ctaSendPerEmailLabel}</a>
              </div>
              <div className='callToAction-groupLinks'>
                <a href='#' className='callToAction-link btn-sodexo btn-sodexo-white' onClick={this.onClickTypeform}>{this.state.ctaBeContactedLabel}</a>
                <a href={this.state.ctaPLaceanOrderLink} className='callToAction-link callToAction-link-order btn-sodexo btn-sodexo-red' target='_blank'>{this.state.ctaPLaceanOrderLabel}</a>
              </div>
            </div>
            <p className='callToAction-disclaimer'>{this.state.ctaDisclaimer}</p>
          </section>

          {/* <script dangerouslySetInnerHTML={this.addTypeformScript()}></script> */}
        </div>
      )
    }
  }
}

export default SpainSalaryApp
