import React from 'react'
import PropTypes from 'prop-types'

import styles from './counter.scss'

export default class Counter extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      value: (this.props.value < this.props.min) ? this.props.min : (this.props.value || 0),
      min: this.props.min || 0,
      max: this.props.max || -1,
      step: this.props.step || 1,
      stepInput: this.props.stepInput || 1,
      prefix: this.props.prefix || '',
      suffix: this.props.suffix || '',
      currency: this.props.currency || false,
      minDisabled: '',
      maxDisabled: '',
      error: false
    }
  }

  componentWillReceiveProps (nextProps) {
    // console.log(this.state.max, nextProps.max)

    this.setState({ max : nextProps.max })

    // if (nextProps.max  >= this.state.max) {
    //   this.setState({ maxDisabled : 'counter-disabled' })
    // } else {
    //   this.setState({ maxDisabled : '' })
    //   this.setState({ minDisabled : '' })
    // }

    // if (nextProps.min <= this.state.min) {
    //   this.setState({ minDisabled : 'counter-disabled' })
    // } else {
    //   this.setState({ minDisabled : '' })
    //   this.setState({ maxDisabled : '' })
    // }
  }

  componentWillMount = () => {
    console.log(this.state.max)

    if (this.state.value === this.state.min) {
      this.setState({ minDisabled : 'counter-disabled' })
    }

    if (this.state.value === this.state.max) {
      this.setState({ maxDisabled : 'counter-disabled' })
    }
  }

  componentDidMount = () => {
    // let input = document.getElementsByClassName('counter-value')
    // if ((this.state.prefix !== '') || (this.state.suffix !== '')) {
    //   for (let i = 0; i < input.length; i++) {
    //     input[i].style.width = ((input[i].value.length + 1) * 10) + 'px'
    //   }
    // }
  }

  set = (value) => {
    this.setState({
      value
    })
    this.props.onChange(value)
  }

  _onChange = (e) => {
    let newValue = e.target.value
    // console.log(newValue, this.state.max, this.state.min, newValue > this.state.max, newValue < this.state.min)
    // check for empty string or invalid values
    this.state.min = this.state.min * 1
    this.state.max = this.state.max * 1
    if (newValue === '') {
      this.set('') // fallback to min value
      this.setState({ error : true })
    } else if ((newValue*1 > this.state.max && this.state.max !== -1) || newValue*1 < this.state.min) {
      // this.set(newValue)
      this.setState({ error : true })
    } else if (typeof newValue !== 'number') {
      this.set(newValue*1) // fix force string to number
      newValue = newValue * 1

      if (newValue >= this.state.max) {
        this.setState({ maxDisabled : 'counter-disabled' })
      } else {
        this.setState({ maxDisabled : '' })
      }

      if (newValue <= this.state.min) {
        this.setState({ minDisabled : 'counter-disabled' })
      } else {
        this.setState({ minDisabled : '' })
      }
      this.setState({ error : false })
    }
  }

  __precisionRound(number, precision) {
    var factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
  }

  _increase = (value) => {

    if (value === '') {
      this.set(this.state.min) // fallback to min value
    } else {
      let parsed = parseFloat(value)
      // if parsed is not a number
      if (isNaN(parsed)) {
        this.set(this.state.min) // fallback to min value
      } else {
        if (value < this.state.max || this.state.max === -1) {
          if ((parsed + this.state.step) <= this.state.max) {
            this.set(this.__precisionRound(parsed + this.state.step, 2)) // increment value
            if (parsed + this.state.step >= this.state.max) {
              this.setState({ maxDisabled : 'counter-disabled' })
            } else {
              this.setState({ maxDisabled : '' })
              this.setState({ minDisabled : '' })
            }
            this.setState({ error : false })
          }
        }
      }
    }
  }

  _decrease = (value) => {
    if (value === '') {
      this.set(this.state.min) // fallback to min value
    } else {
      let parsed = parseFloat(value)

      // if parsed is not a number
      if (isNaN(parsed)) {
        this.set(this.state.min) // fallback to min value
      } else {
        if (value > this.state.min) {
          if ((parsed - this.state.step) >= this.state.min) {
            this.set(this.__precisionRound(parsed - this.state.step, 2)) // increment value
            if (parsed - this.state.step <= this.state.min) {
              this.setState({ minDisabled : 'counter-disabled' })
            } else {
              this.setState({ minDisabled : '' })
              this.setState({ maxDisabled : '' })
            }
            this.setState({ error : false })
          }
        }
      }
    }
  }

  onKeyPress (event) {
    if ((this.state.prefix !== '') || (this.state.suffix !== '')) {
      event.target.style.width = ((event.target.value.length + 1) * 10) + 'px'
    }
  }

  onKeyDown (event) {
    if ((this.state.prefix !== '') || (this.state.suffix !== '')) {
      if (event.keyCode === 8 && event.target.value.length !== 1) {
        event.target.style.width = ((event.target.value.length - 1) * 10) + 'px'
      }
    }
  }

  render () {
    const { value } = this.state

    let withPrefix = this.state.prefix !== '' ? ' counter-with_prefix' : ''
    let withSuffix = this.state.suffix !== '' ? ' counter-with_suffix' : ''

    let error = this.state.error ? ' error-counter' : ''

    return (
      <div className={'counter' + error}>
        <div
          className={'counter-decrease ' + this.state.minDisabled}
          onClick={() => { this._decrease(value) }}>
          <span>-</span>
        </div>

        <div className='counter-input'>
          {/* <i>{this.state.prefix}</i> */}
          <input
            className={'counter-value' + withPrefix + withSuffix}
            //onKeyPress={this.onKeyPress}
            //onKeyDown={this.onKeyDown}
            type='number'
            step={this.state.stepInput}
            onChange={this._onChange}
            value={this.state.value}
          />
          {/* <i>{this.state.suffix}</i> */}
        </div>

        <div className={'counter-increase ' + this.state.maxDisabled} onClick={() => { this._increase(value) }}><span>+</span></div>
      </div>
    )
  }
}

Counter.propTypes = {
  value: PropTypes.any,
  min: PropTypes.any,
  max: PropTypes.any,
  step: PropTypes.any,
  stepInput: PropTypes.any,
  suffix: PropTypes.any,
  prefix: PropTypes.any,
  currency: PropTypes.bool,
  onChange: PropTypes.func,
}

Counter.defaultProps = {
  value: 0,
  min: 0,
  max: 10,
  step: 1,
  stepInput: 1,
  suffix: '',
  prefix: '',
  currency: false
}
