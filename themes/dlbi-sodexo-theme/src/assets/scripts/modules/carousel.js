/**
 * Carousel scripts
 */
 /*
(function($) {

    $.fn.carouselHeights = function() {

        var items = $(this), //grab all slides
            heights = [], //create empty array to store height values
            tallest; //create variable to make note of the tallest slide

        var normalizeHeights = function() {

            items.each(function() { //add heights to array
                heights.push($(this).height());
            });
            tallest = Math.max.apply(null, heights); //cache largest value
            items.each(function() {
                $(this).css('min-height',tallest + 'px');
            });
        };

        normalizeHeights();

        $(window).on('resize orientationchange', function () {
            //reset vars
            tallest = 0;
            heights.length = 0;

            items.each(function() {
                $(this).css('min-height','0'); //reset min-height
            });
            normalizeHeights(); //run it again
        });

    };


    var carouselTopMenu = $('#top-menu');
    var carouselItemsTopMenu = carouselTopMenu.find('.carousel-item ');

    carouselTopMenu.carousel({
      interval: false
    }).on('slid.bs.carousel', function (event) {
        $('.top-menu--homepage').children().siblings('.active').removeClass('active')
        $('.top-menu--homepage').children().eq(carouselItemsTopMenu.siblings('.active').index()).addClass('active') ;


        $('.top-menu--header_title').children().siblings('.active').removeClass('active')
        $('.top-menu--header_title').children().eq(carouselItemsTopMenu.siblings('.active').index()).addClass('active') ;
    })


    var carouselSingle = $('#single-carousel');
    carouselSingle.carousel({
      interval: false
    });

})(jQuery);
*/
