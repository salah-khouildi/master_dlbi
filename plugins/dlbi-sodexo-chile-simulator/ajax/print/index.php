<?php
// Load WordPress
$wp_load = $_SERVER['DOCUMENT_ROOT'] . '/wp/wp-load.php';
require_once( $wp_load );

$numberofemployees;
$dailyamount;
$discountVat;
$netvatcost;
$dailysavings;
$percentofsaving;
$realcost;
$costwithoutchequeresto;

$logo      = get_field( 'simulator_logo', 'option' );
$link      = get_field( 'simulator_link', 'option' );
$fee_label = get_field( 'pdf_delivery_fee_label', 'option' );

if ( isset( $_GET ) && ( ! empty( $_GET ) ) ) {
	$simulatorvalue = $_GET; // Permit to change the method easily

	if ( ( array_key_exists( 'annual', $simulatorvalue ) ) && ( ! empty( $simulatorvalue['annual'] ) ) ) {
		$annual = $simulatorvalue['annual'];
	};
	if ( ( array_key_exists( 'numberofemployees', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['numberofemployees'] ) ) && ( ! empty( $simulatorvalue['numberofemployees'] ) ) ) {
		$numberofemployees = $simulatorvalue['numberofemployees'];
	}
	if ( ( array_key_exists( 'dailyamount', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['dailyamount'] ) ) && ( ! empty( $simulatorvalue['dailyamount'] ) ) ) {
		$dailyamount = $simulatorvalue['dailyamount'];
	}
	if ( ( array_key_exists( 'discountvat', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['discountvat'] ) ) && ( ! empty( $simulatorvalue['discountvat'] ) ) ) {
		$discountVat = ( $annual == 'true' ) ? $simulatorvalue['discountvat'] * 12 : $simulatorvalue['discountvat'];
	}
	if ( ( array_key_exists( 'netvatcost', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['netvatcost'] ) ) && ( ! empty( $simulatorvalue['netvatcost'] ) ) ) {
		$netvatcost = ( $annual == 'true' ) ? ( $simulatorvalue['netvatcost'] ) * 12 : $simulatorvalue['netvatcost'];
	}
	if ( ( array_key_exists( 'dailysavings', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['dailysavings'] ) ) && ( ! empty( $simulatorvalue['dailysavings'] ) ) ) {
		$dailysavings = ( $annual == 'true' ) ? $simulatorvalue['dailysavings'] * 12 : $simulatorvalue['dailysavings'];
	}
	if ( ( array_key_exists( 'percentofsaving', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['percentofsaving'] ) ) && ( ! empty( $simulatorvalue['percentofsaving'] ) ) ) {
		$percentofsaving = $simulatorvalue['percentofsaving'];
	}
	if ( ( array_key_exists( 'realcost', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['realcost'] ) ) && ( ! empty( $simulatorvalue['realcost'] ) ) ) {
		$realcost = ( $annual == 'true' ) ? $simulatorvalue['realcost'] * 12 : $simulatorvalue['realcost'];
	}
	if ( ( array_key_exists( 'costwithoutchequeresto', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['costwithoutchequeresto'] ) ) && ( ! empty( $simulatorvalue['costwithoutchequeresto'] ) ) ) {
		$costwithoutchequeresto = ( $annual == 'true' ) ? $simulatorvalue['costwithoutchequeresto'] * 12 : $simulatorvalue['costwithoutchequeresto'];
	}
	$date_text = ( $annual == 'true' ) ? 'Anual' : 'Mensual';
}
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Print your simulator</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="theme-color" content="#283897">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="styles/style.css">
</head>

<body id="page">
<div class="container-fluid">
    <div class="row" id="logo">
		<?php if ( $logo ): ?>
            <div class="col-md-3"><h1><img src="<?php echo $logo['sizes']['large'] ?>" width="200" height="70" alt="Sodexo"/></h1></div>
		<?php else: ?>
            <div class="col-md-3"><h1><img src="img/logo-sodexo-header.svg" width="200" height="70" alt="Sodexo"/></h1></div>
		<?php endif; ?>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered" id="infos">
                <thead>
                <tr>
                    <th class="active">Candidad de empleados</th>
                    <th class="active">Valor diario de colación($)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?php if ( ! empty( $numberofemployees ) ) {
							echo $numberofemployees;
						} ?></td>
                    <td><?php if ( ! empty( $dailyamount ) ) {
							echo $dailyamount;
						} ?></td>
                </tr>
                </tbody>
            </table>
            <table class="table table-bordered" id="result">
                <tbody>
                <tr>
                    <td class="info"><b>Resultado <?php echo $date_text ?>: </b></td>
                    <td class="info"><b>Ahorro IVA 19% : </b></td>
                    <td>$ <?php if ( ! empty( $discountVat ) ) {
							echo number_format( $discountVat, 0, ',', ' ' );
						} ?></td>
                    <td class="info"><b>Ahorro impuesto a la renta 25%</b></td>
                    <td>$ <?php if ( ! empty( $netvatcost ) ) {
							echo number_format( $netvatcost, 0, ',', ' ' );
						} ?></td>
                    <td class="info"><b>Con Cheque Restaurant ahorra $ </b></td>
                    <td>$ <?php if ( ! empty( $dailysavings ) ) {
							echo number_format( $dailysavings, 0, ',', ' ' );
						} ?></td>
                    <td class="info"><b>Con Cheque Restaurant ahorra % </b></td>
                    <td><?php if ( ! empty( $percentofsaving ) ) {
							echo number_format( $percentofsaving, 2, ',', ' ' );
						} ?> %
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="table table-bordered" id="economy">
                <tbody>
                <tr>
                    <td>Costo con Cheque Restaurant : $ <?php if ( ! empty( $realcost ) ) {
							echo number_format( $realcost, 0, ',', ' ' );
						} ?></td>


                </tr>
                <tr>
                    <td>Costo sin ahorro : $ <?php if ( ! empty( $costwithoutchequeresto ) ) {
							echo number_format( $costwithoutchequeresto, 0, ',', ' ' );
						} ?></td>


                </tr>
                </tbody>
            </table>
	        <?php if ( $fee_label ) { ?>
                <table class="delivrery-fee" id="delivrery-fee">
                    <tbody>
                    <tr>
                        <td><?= $fee_label ?></td>
                    </tr>
                    </tbody>
                </table>
	        <?php } ?>
        </div>
    </div>
	<?php if ( $link ): ?>
        <div class="row" id="advantage"><?php echo $link['url'] ?></div>
	<?php else: ?>
        <div class="row" id="advantage">Sodexo advantages</div>
	<?php endif ?>
</div>
</body>
</html>
