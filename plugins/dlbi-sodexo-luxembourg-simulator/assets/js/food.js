
export default class FoodSimulator {

    constructor() {
	// Inputs
	this.salario_medio = 15000;
	this.importe_diario = 9;
	this.numero_emplea_medio = 35;

	// Values from database
	//    tax_bracket_first_level = get_field('tax_bracket_first_level', 'option');
	//    tax_payed_up_to_each_bracket_level_1 = get_field('tax_payed_up_to_each_bracket_level_1', 'option');
	//    remainder_level_1 = get_field('remainder_level_1', 'option');
	//    marginal_tax_rate_level_1 = get_field('marginal_tax_rate_level_1', 'option');
	//    average_tax_rate_level_1 = get_field('average_tax_rate_level_1', 'option');
	//    levels = get_field('levels', 'option');

	this.tax_bracket_first_level = 0;
	this.tax_payed_up_to_each_bracket_level_1 = 0
	this.remainder_level_1 = 12450;
	this.marginal_tax_rate_level_1 = 19;
	this.average_tax_rate_level_1 = 19;
	this.levels = [
	    {
		taxBracket: 0,
		tax_payed_up_to_each_bracket: 0,
		marginal_tax_rate: 0.19
	    },
	    {
		taxBracket: 12450,
		tax_payed_up_to_each_bracket: 2365.50,
		marginal_tax_rate: 0.24
	    },
	    {
		taxBracket: 20200,
		tax_payed_up_to_each_bracket: 4225.5,
		marginal_tax_rate: 0.30
	    },
	    {
		taxBracket: 35200,
		tax_payed_up_to_each_bracket: 8725.5,
		marginal_tax_rate: 0.37
	    },
	    {
		taxBracket: 60000,
		tax_payed_up_to_each_bracket: 17901.5,
		marginal_tax_rate: 0.45
	    }
	]

	this.response = this.sodexo_return_response()
    }

    sodexo_simulator_sumprod(array, index1, index2) {
	let result = 0;
	for (let i = 0; i < array.length; i++) {
	    result += array[i][index1] * array[i][index2];
	}
	return result;
    }

    sodexo_simulator_tipo_hasta_IRPF_medio(average_wage, tax_bracket_average_tax_rate) {
	let tipo_hasta_IRPF_medio = 0;
	let index = 0;
	let size = tax_bracket_average_tax_rate.length;
	do {
	    if (average_wage < tax_bracket_average_tax_rate[1]['tax_bracket']) {
		tipo_hasta_IRPF_medio = 0;
	    } else {
		tipo_hasta_IRPF_medio = tax_bracket_average_tax_rate[index]['average_tax_rate'];
	    }

	    index++;
	} while (index < size && average_wage >= tax_bracket_average_tax_rate[index]['tax_bracket']);

	return tipo_hasta_IRPF_medio;
    }

    sodexo_simulator_tipo_resto_IRPF_medio(irpf_medio, average_tax_rate) {
	if (irpf_medio == 0) {
	    return  get_field('marginal_tax_rate_level_1', 'option') / 100;
	} else if (irpf_medio && average_tax_rate) {
	    for (let i = 0; i < average_tax_rate.length; i++) {
		if (irpf_medio == average_tax_rate[i]['average_tax_rate']) {
		    return average_tax_rate[i]['marginal_tax_rate'];
		}
	    }
	}
    }

    sodexo_simulator_base_hasta_IRPF_medio(tipo_resto_IRPF_medio, average_tax_rate) {
	if (tipo_resto_IRPF_medio == 0) {
	    return 0;
	} else if (tipo_resto_IRPF_medio && average_tax_rate) {
	    for (let i = 0; average_tax_rate.length; i++) {
		if (tipo_resto_IRPF_medio == average_tax_rate[i]['marginal_tax_rate']) {
		    return average_tax_rate[i]['tax_bracket'];
		}
	    }
	}
    }

    sodexo_simulator_base_resto_IRPF_medio(salario_medio, tipo_hasta_IRPF_medio, average_tax_rate) {
	if (tipo_hasta_IRPF_medio == 0) {
	    return  salario_medio;
	} else if (tipo_hasta_IRPF_medio && average_tax_rate && salario_medio) {
	    for (let i = 0; average_tax_rate.length; i++) {
		if (tipo_hasta_IRPF_medio == this.average_tax_rate_level_1) {
		    return salario_medio;
		} else if (tipo_hasta_IRPF_medio == average_tax_rate[i]['average_tax_rate']) {
		    return salario_medio - average_tax_rate[i]['tax_bracket'];
		}
	    }
	}
    }

    sodexo_return_response() {

	let taxes_and_rates_one = [];
	taxes_and_rates_one['tax_bracket'] = this.tax_bracket_first_level;
	taxes_and_rates_one['tax_payed_up_to_each_bracket'] = this.tax_payed_up_to_each_bracket_level_1 / 100;
	taxes_and_rates_one['remainder'] = this.remainder_level_1;
	taxes_and_rates_one['marginal_tax_rate'] = this.marginal_tax_rate_level_1 / 100;
	taxes_and_rates_one['average_tax_rate'] = this.average_tax_rate_level_1;

	let taxes_and_rates = [];
	taxes_and_rates = [taxes_and_rates_one]


	let total_ticket_per_year = this.importe_diario * 220;
	let wage_and_tickets = this.salario_medio + total_ticket_per_year;

	let index = 1;
	let levels_size = this.levels.length;
	console.log(this.levels.length)
	for (let i = 0; i < this.levels.length; i++) {
	    let line_tax_and_rates = [];

	    line_tax_and_rates['tax_bracket'] = this.levels[i]['tax_payed_up_to_each_bracket'];
	    line_tax_and_rates['tax_payed_up_to_each_bracket'] = taxes_and_rates[index - 1]['tax_payed_up_to_each_bracket'] + (taxes_and_rates[index - 1]['remainder'] * taxes_and_rates[index - 1]['marginal_tax_rate']);
	    if (index < levels_size) {
		line_tax_and_rates['remainder'] = this.levels[index]['tax_payed_up_to_each_bracket'] - line_tax_and_rates['tax_bracket'];
	    } else {
		line_tax_and_rates['remainder'] = '';
	    }
	    line_tax_and_rates['marginal_tax_rate'] = this.levels[i]['marginal_tax_rate'] / 100;
	    taxes_and_rates.push(line_tax_and_rates);
	    if (index < levels_size) {
		let sum_prod = this.sodexo_simulator_sumprod(taxes_and_rates, 'remainder', 'marginal_tax_rate');
		let rate = sum_prod / this.levels[index]['tax_payed_up_to_each_bracket'];
		line_tax_and_rates['average_tax_rate'] = Math.ceil(rate * 100);
	    } else {
		line_tax_and_rates['average_tax_rate'] = 34;
	    }
	    taxes_and_rates[index]['average_tax_rate'] = line_tax_and_rates['average_tax_rate'];
	    index++;
	}

	let tipo_hasta_IRPF_medio = this.sodexo_simulator_tipo_hasta_IRPF_medio(this.salario_medio, taxes_and_rates);
	let tipo_hasta_IRPF_ticket = this.sodexo_simulator_tipo_hasta_IRPF_medio(wage_and_tickets, taxes_and_rates);
	let tipo_resto_IRPF_medio = this.sodexo_simulator_tipo_resto_IRPF_medio(tipo_hasta_IRPF_medio, taxes_and_rates);
	let tipo_resto_IRPF_ticket = this.sodexo_simulator_tipo_resto_IRPF_medio(tipo_hasta_IRPF_ticket, taxes_and_rates);
	let base_hasta_IRPF_medio = this.sodexo_simulator_base_hasta_IRPF_medio(tipo_resto_IRPF_medio, taxes_and_rates);
	let base_hasta_IRPF_ticket = this.sodexo_simulator_base_hasta_IRPF_medio(tipo_resto_IRPF_ticket, taxes_and_rates);
	let base_resto_IRPF_medio = this.sodexo_simulator_base_resto_IRPF_medio(this.salario_medio, tipo_hasta_IRPF_medio, taxes_and_rates);
	let base_resto_IRPF_ticket = this.sodexo_simulator_base_resto_IRPF_medio(wage_and_tickets, tipo_hasta_IRPF_ticket, taxes_and_rates);
	let retencion_IRPF_medio = Math.ceil(base_hasta_IRPF_medio * (tipo_hasta_IRPF_medio / 100) + base_resto_IRPF_medio * tipo_resto_IRPF_medio);
	let retencion_IRPF_ticket = Math.ceil(base_hasta_IRPF_ticket * (tipo_hasta_IRPF_ticket / 100) + base_resto_IRPF_ticket * tipo_resto_IRPF_ticket);
	let difference = Math.ceil(retencion_IRPF_ticket - retencion_IRPF_medio);
	let ticket_difference = Math.ceil(total_ticket_per_year - difference);
	let taxes_saved = Math.ceil((difference / total_ticket_per_year) * 100);
	let company_spent = Math.ceil(total_ticket_per_year * this.numero_emplea_medio);
	let employees_save = Math.ceil(this.numero_emplea_medio * difference);

	debugger;

	let response = {
	    retencion_IRPF_medio: retencion_IRPF_medio,
	    retencion_IRPF_ticket: retencion_IRPF_ticket,
	    taxes_saved: taxes_saved,
	    company_spent: company_spent,
	    employees_save: employees_save
	}



	return response;



    }

}
