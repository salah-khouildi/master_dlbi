<?php
// Load WordPress
$wp_load = $_SERVER['DOCUMENT_ROOT'] . '/wp/wp-load.php';
require_once( $wp_load );

$nombresalaries;
$nombreticketsresto;
$valeurtitre;
$valeurparticipationemployeur;
$budgettotal;
$participationemployeur;
$cofinancement;
$pouvoirachatadditionnel;
$economiesrealiseesemployeur;
$gainemployee;

$logo      = get_field( 'simulator_logo', 'option' );
$link      = get_field( 'simulator_link', 'option' );
$fee_label = get_field( 'pdf_delivery_fee_label', 'option' );

if ( isset( $_GET ) && ( ! empty( $_GET ) ) ) {
	$simulatorvalue = $_GET; // Permit to change the method easily
// france
	if ( ( array_key_exists( 'nombresalaries', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['nombresalaries'] ) ) && ( ! empty( $simulatorvalue['nombresalaries'] ) ) ) {
		$nombresalaries = $simulatorvalue['nombresalaries'];
	}
	if ( ( array_key_exists( 'nombreticketsresto', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['nombreticketsresto'] ) ) && ( ! empty( $simulatorvalue['nombreticketsresto'] ) ) ) {
		$nombreticketsresto = $simulatorvalue['nombreticketsresto'];
	}
	if ( ( array_key_exists( 'valeurtitre', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['valeurtitre'] ) ) && ( ! empty( $simulatorvalue['valeurtitre'] ) ) ) {
		$valeurtitre = $simulatorvalue['valeurtitre'];
	}
	if ( ( array_key_exists( 'valeurparticipationemployeur', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['valeurparticipationemployeur'] ) ) && ( ! empty( $simulatorvalue['valeurparticipationemployeur'] ) ) ) {
		$valeurparticipationemployeur = ( $simulatorvalue['valeurparticipationemployeur'] * 100 );
	}
	if ( ( array_key_exists( 'budgettotal', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['budgettotal'] ) ) && ( ! empty( $simulatorvalue['budgettotal'] ) ) ) {
		$budgettotal = $simulatorvalue['budgettotal'];
	}
	if ( ( array_key_exists( 'participationemployeur', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['participationemployeur'] ) ) && ( ! empty( $simulatorvalue['participationemployeur'] ) ) ) {
		$participationemployeur = $simulatorvalue['participationemployeur'];
	}
	if ( ( array_key_exists( 'cofinancement', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['cofinancement'] ) ) && ( ! empty( $simulatorvalue['cofinancement'] ) ) ) {
		$cofinancement = $simulatorvalue['cofinancement'];
	}
	if ( ( array_key_exists( 'pouvoirachatadditionnel', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['pouvoirachatadditionnel'] ) ) && ( ! empty( $simulatorvalue['pouvoirachatadditionnel'] ) ) ) {
		$pouvoirachatadditionnel = $simulatorvalue['pouvoirachatadditionnel'];
	}
	if ( ( array_key_exists( 'economiesrealiseesemployeur', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['economiesrealiseesemployeur'] ) ) && ( ! empty( $simulatorvalue['economiesrealiseesemployeur'] ) ) ) {
		$economiesrealiseesemployeur = $simulatorvalue['economiesrealiseesemployeur'];
	}
	if ( ( array_key_exists( 'gainemployee', $simulatorvalue ) ) && ( is_numeric( $simulatorvalue['gainemployee'] ) ) && ( ! empty( $simulatorvalue['gainemployee'] ) ) ) {
		$gainemployee = $simulatorvalue['gainemployee'];
	}

	if ( array_key_exists( 'annual', $simulatorvalue ) ) {
		$is_annual = $simulatorvalue['annual'];
	}

	if ( array_key_exists( 'lang', $simulatorvalue ) ) {
		$lang = $simulatorvalue['lang'];
	}

	if ( array_key_exists( 'simulator', $simulatorvalue ) ) {
		if ( $simulatorvalue['simulator'] == 'romaniaMeal' ) {
			$current_simulator = 'rom_meal_';
		} else {
			$current_simulator = 'rom_gift_';
		}
	}

	$lang_slug = $lang == 'ro-RO' ? '/' : '/en/';
	$json      = file_get_contents( site_url() . $lang_slug . 'wp-json/acf/v2/options/' );
	$datas     = json_decode( $json );
}
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Print your simulator</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="theme-color" content="#283897">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="styles/style.css">
</head>

<body id="page">
<div class="container-fluid">
    <div class="row" id="logo">
		<?php if ( $logo ): ?>
            <div class="col-md-3"><h1><img src="<?php echo $logo['sizes']['large'] ?>" width="200" height="70" alt="Sodexo"/></h1></div>
		<?php else: ?>
            <div class="col-md-3"><h1><img src="img/logo-sodexo-header.svg" width="200" height="70" alt="Sodexo"/></h1></div>
		<?php endif; ?>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered" id="infos">
                <thead>
                <tr>
                    <th class="active"><?php $number_of_employee = $current_simulator . 'number_of_employees';
						echo $datas->acf->$number_of_employee; ?></th>
                    <th class="active"><?php $number_of_vouchers = $current_simulator . 'number_of_vouchers';
						echo $datas->acf->$number_of_vouchers; ?></th>
                    <th class="active"><?php $facial_value_of_the_voucher = $current_simulator . 'facial_value_of_the_voucher';
						echo $datas->acf->$facial_value_of_the_voucher; ?></th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?php if ( ! empty( $nombresalaries ) ) {
							echo $nombresalaries;
						} ?></td>
                    <td><?php if ( ! empty( $nombreticketsresto ) ) {
							echo $nombreticketsresto;
						} ?></td>
                    <td><?php if ( ! empty( $valeurtitre ) ) {
							echo $valeurtitre;
						} ?></td>

                </tr>
                </tbody>
            </table>
            <table class="table table-bordered" id="result">
                <tbody>
                <tr>
                    <td class="info"><b><?php $rezultate = $current_simulator . 'rezultate';
							echo $datas->acf->$rezultate; ?> : </b></td>
                    <td><?php $an = $current_simulator . 'anual';
						$mo       = $current_simulator . 'monthly';
						echo ( $is_annual == "true" ) ? $datas->acf->$an : $datas->acf->$mo ?></td>
                    <td class="info"><b><?php $total_spending_for_the_employer = $current_simulator . 'total_spending_for_the_employer';
							echo $datas->acf->$total_spending_for_the_employer; ?> : </b></td>
                    <td><?php if ( ! empty( $budgettotal ) ) {
							echo number_format( $budgettotal, 2, ',', ' ' );
						} ?> LEI
                    </td>
                    <td class="info"><b><?php $income_tax_employee = $current_simulator . 'income_tax_employee';
							echo $datas->acf->$income_tax_employee; ?> : </b></td>
                    <td><?php if ( ! empty( $participationemployeur ) ) {
							echo number_format( $participationemployeur, 2, ',', ' ' );
						} ?> LEI
                    </td>
                    <!-- <td class="info"><b>Coût pour le salarié : </b></td>
				<td><?php //if (!empty($cofinancement)) echo number_format($cofinancement, 2, ',', ' '); ?> €</td> -->
                </tr>
                </tbody>
            </table>
            <table class="table table-bordered" id="donut">
                <tbody>
                <tr>
                    <td><b><?php $donut = $current_simulator . 'donut';
							echo $datas->acf->$donut; ?> : </b><?php if ( ! empty( $pouvoirachatadditionnel ) ) {
							echo number_format( $pouvoirachatadditionnel, 2, ',', ' ' );
						} ?> %
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="table table-bordered" id="economy">
                <thead>
                </thead>
                <tbody>
                <tr>
                    <td><?php $savings_for_the_employer = $current_simulator . 'savings_for_the_employer';
						echo $datas->acf->$savings_for_the_employer; ?> : <?php if ( ! empty( $economiesrealiseesemployeur ) ) {
							echo number_format( $economiesrealiseesemployeur, 2, ',', ' ' );
						} ?> LEI
                    </td>
                </tr>
                <!-- <tr>
				<td>Pour le salarié : <?php //if (!empty($gainemployee)) echo number_format($gainemployee, 2, ',', ' '); ?> €</td>


			    </tr> -->
                </tbody>
            </table>
	        <?php if ( $fee_label ) { ?>
                <table class="delivrery-fee" id="delivrery-fee">
                    <tbody>
                    <tr>
                        <td><?= $fee_label ?></td>
                    </tr>
                    </tbody>
                </table>
	        <?php } ?>
        </div>
    </div>
	<?php if ( $link ): ?>
        <div class="row" id="advantage"><?php echo $link['url'] ?></div>
	<?php else: ?>
        <div class="row" id="advantage">Sodexo advantages</div>
	<?php endif ?>
</div>
</body>
</html>
