<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */
// $the_theme = wp_get_theme();
// $container = get_theme_mod( 'understrap_container_type' );
// $fb_page = get_field('facebook_page', 'option');
// $twitter_page = get_field('twitter_page', 'option');
// $google_plus_page = get_field('google_plus_page', 'option');
// $youtube_page = get_field('youtube_page', 'option');

if (is_multisite()) {
    $blog_list = get_sites(0, 'all');
}
$logo_footer = get_field('logo_footer', 'option');
$logo_url_footer = get_field('logo_url_footer', 'option');

$simplelink = 'Simple link';
$typeform = 'Typeform';
$youtube = 'Youtube';
$show_wcb = get_field('web_callback_show', 'option');
$wcb_id = get_field('web_callback_id', 'option');
$show_custom_wcb = get_field('lmt_show_web_call_back', get_the_ID());
$wcb_custom_id = get_field('lmt_web_call_back_id', get_the_ID());
$is_rp_content = get_field('is_rp_content', 'option');
?>
<?php // get_sidebar( 'footerfull' );                      ?>
<?php //echo sodexo_display_cart()            ?>
<div id="wrapper-footer">
    <!-- Return to Top -->
    <a href="#" id="return-to-top"><i class="fa fa-angle-up"></i></a>

    <input type="hidden" id="acf_link_country" name="toto" value="<?php echo untrailingslashit(site_url()) ?>" />
    <div class="footer-top">

        <div class="container">

            <div class="row">

                <div class="col-md-4">
                    <div class="footer-col">
                        <?php if (is_active_sidebar('footer-1')) : ?>
                            <ul>
                                <?php dynamic_sidebar('footer-1'); ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="footer-col">
                        <?php if (is_active_sidebar('footer-2')) : ?>
                            <?php dynamic_sidebar('footer-2'); ?>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="col-md-4">

                    <div class="footer-col">
                        <?php if (is_active_sidebar('footer-3')) : ?>
                            <?php dynamic_sidebar('footer-3'); ?>
                        <?php endif; ?>
                    </div>

                </div>

            </div>

        </div>

    </div>
    <?php if (!$is_rp_content): ?>
        <div class="footer-bottom">

            <div class="container">

                <div class="row align-items-center">
                        <div class="col-md-10">
                            <div class="footer-bottom-menu">
                                <?php wp_nav_menu(array('theme_location' => 'footer-nav')) ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="date-copy">
                                © SODEXO <?php echo date('Y'); ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- .footer-bottom -->
            <?php if ($logo_footer): ?>
                <div class="footer-last">
                    <?php
                    if (get_field('logo_select_a_link_type', 'option') == $simplelink):
                        if (is_array(get_field('logo_url_footer', 'option'))):
                            ?>
                            <a href="<?php echo $logo_url_footer["url"] ?>" <?php if ($logo_url_footer["target"]): ?> target="_blank" <?php endif; ?>>
                            <?php endif ?>
                        <?php elseif (get_field('logo_select_a_link_type', 'option') == $typeform) : ?>
                            <a href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $logo_url_footer["url"] ?>">
                            <?php elseif (get_field('logo_select_a_link_type', 'option') == $youtube) : ?>
                                <a data-fancybox href="<?php echo $logo_url_footer["url"] ?>" >
	                                <?php endif;
	                                echo apply_filters( "dlbi_image", $logo_footer, "confianza", "", "", 186, 65 ); ?>
	                                <?php if ( get_field( 'logo_select_a_link_type', 'option' ) ): ?>
                                </a>
                            <?php endif; ?>
                            </div>
                        <?php endif
                        ?>
                        </div><!-- #wrapper-footer -->
                    <?php endif ?>

                    <!-- Start Lead Management Toolbar Options -->
                    <div id="lead-management-tools">
                        <?php // get_template_part('layouts-toolbar-options/block-search'); // Load search layout    ?>
                        <?php // get_template_part('layouts-toolbar-options/block-call-back'); // Load call back layout    ?>
                        <?php // get_template_part('layouts-toolbar-options/block-request-a-quote'); // Load request a quote layout ?>

                        <?php get_template_part('layouts-toolbar-options/layout-lead-toolbar'); // Load search layout NEW LMT     ?>
                    </div>
                    <div id="lmt-overlay"></div>
                    <!-- End Lead Management Toolbar Options -->

                    </div><!-- #page -->

                    <div id="outdated">
                        <h6><?php echo __('Your browser is out-of-date!', 'lbi-sodexo-theme'); ?></h6>
                        <p><?php echo __('Update your browser to view this website correctly.', 'lbi-sodexo-theme'); ?> <a id="btnUpdateBrowser" href="http://outdatedbrowser.com/"><?php echo __('Update my browser now', 'lbi-sodexo-theme'); ?> </a></p>
                        <p class="last"><a href="#" id="btnCloseUpdateBrowser" title="Close">&times;</a></p>
                    </div>

                    <?php wp_footer(); ?>

                    <div class="modal fade" id="typeformModal" tabindex="-1" role="dialog" aria-labelledby="typeformModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span class="sr-only">Close overlay</span>
                                    </button>
                                </div>
                                <div class="modal-body ajax">
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    if ($show_wcb && $wcb_id):
                        if ($show_custom_wcb && $wcb_custom_id) {
                            $wcb_id = $wcb_custom_id;
                        }
                        ?>
                        <script type="text/javascript" src="<?php echo $wcb_id ?>"></script>
                        <?php
                    endif;
                    ?>
                    </body>
                    </html>