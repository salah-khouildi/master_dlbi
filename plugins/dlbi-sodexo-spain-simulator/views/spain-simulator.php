<section class="w-100">
<div class="">
<?php
            if (array_key_exists('widget_title', $atts)) : ?>
<p class="sodexo-pretitle"><?php echo $atts['widget_title']; ?></p>
<?php endif; ?>
<div id="root" style="height: 100%"> </div>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#sendmailmodal">
  Send by mail
</button>

<!-- Modal -->
<div class="modal fade"  id="sendmailmodal" tabindex="-1" role="dialog" aria-labelledby="sendmailmodal" aria-hidden="true">>
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Title ACF à faire</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      <div class="modal-body">
        <p>Message ACF à Faire</p>
        <form action="https://advantage.master.dev.soxodlbi.xyz/app/plugins/dlbi-sodexo-spain-simulator/ajax/pdf.php?socialSecurityTaxes=1&totalFacialBenefits=2&totalAnnualTaxWithoutSalarySacrifice=134" method="post">
          <div class="form-group">
            <div class="input-group">
              <input type="email" name="send_pdf_mail" class="form-control" id="send_pdf" aria-describedby="sendPdfByMail" placeholder="Your adress">
              <span class="input-group-btn">
              <button class="btn btn-secondary" type="submit">OK</button>
              </span> <small id="sendPdfByMail" class="form-text sr-only">Please enter your email</small> </div>
          </div>
          <p></p>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>
</section>
