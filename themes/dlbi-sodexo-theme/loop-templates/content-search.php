<?php
/**
 * Search results partial template.
 *
 * @package understrap
 */
?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

    <?php
    // Get category article
    $categories = get_the_category();
    if (!empty($categories)) :
	?>  
        <p class="blog-component--container_label"><?php echo esc_html($categories[0]->name); ?></p>
    <?php elseif (get_post_type() == 'faq'): ?>
        <p class="blog-component--container_label"><?php echo __('FAQ', 'lbi-sodexo-theme'); ?></p>
    <?php endif; ?>

    
        <?php if (get_post_type() != 'faq'): ?>
        <div class="post-content">
            <div class="post-heading">
                <?php the_title(sprintf('<h4 class="post-title"><a href="%s" class="search-title" rel="bookmark">', esc_url(get_permalink())), '</a></h4>');?>
            </div>
            <div class="search-body">
                <?php echo wp_trim_words(strip_shortcodes(get_the_content()),30); ?>
            </div>
        </div>
        <?php endif ?>

       
    <?php
    // check if the repeater field has rows of data
    if (have_rows('faq_questions')):
	?>
	    <?php
        $y = 0;
        foreach (get_field('faq_questions') as $question_answer):
            if ($question_answer['faq_question'] != ''):?>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a class="search-title faq-title collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq" href="#collapse<?php echo $this->z.$y; ?>" aria-expanded="true" aria-controls="collapse<?php echo $this->z.$y; ?>">
                        <?php echo $question_answer['faq_question']; ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse<?php echo $this->z.$y; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<?php echo $this->z.$y; ?>">
                <div class="panel-body"><?php echo $question_answer['faq_answer']; ?></div>
                </div>
            </div>
             <?php
                $y++;
            endif;
	    endforeach;
	    ?>
    <?php endif;?>
</article>
