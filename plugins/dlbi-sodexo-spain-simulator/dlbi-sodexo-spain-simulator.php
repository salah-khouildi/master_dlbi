<?php

/*
  Plugin Name: Sodexo Spain simulator
  Description: Plugin for Spain simulator
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: lbi-sodexo-theme
 */

// Plugin Consts
define('SOD_SPA_VERSION', '1.0');
define('SOD_SPA_URL', plugins_url('', __FILE__));
define('SOD_SPA_DIR', dirname(__FILE__));

//Load class
require( SOD_SPA_DIR . '/inc/class-client.php' );
require( SOD_SPA_DIR . '/inc/class-admin.php' );
require( SOD_SPA_DIR . '/inc/widgets/class-spain-simulator.php' );

//Init
function dlbi_sodexo_spain_sim_init() {
    new SodexoSpainSimulator_Client();
    new SodexoWidget_SpainSimulator();

    if(is_admin()){
	new SodexoSpainSimulator_Admin();
    }
    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-sodexo-spain-simulator', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'dlbi_sodexo_spain_sim_init', 11);
