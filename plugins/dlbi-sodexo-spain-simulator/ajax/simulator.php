<?php
header("Access-Control-Allow-Origin: *");
// Load WordPress
$bootstrap = 'wp-load.php';
while (!is_file($bootstrap)) {
    if (is_dir('..'))
	chdir('..');
    else
	die('EN: Could not find WordPress! FR : Impossible de trouver WordPress !');
}
require_once( $bootstrap );

$tax_bracket_first_level = get_field('tax_bracket_first_level', 'option');
$tax_payed_up_to_each_bracket_level_1 = get_field('tax_payed_up_to_each_bracket_level_1', 'option');
$remainder_level_1 = get_field('remainder_level_1', 'option');
$marginal_tax_rate_1 = get_field('marginal_tax_rate_level_1', 'option');
$average_tax_rate_level_1 = get_field('average_tax_rate_level_1', 'option');
$levels = get_field('levels', 'option');

$return = array(
    'tax_bracket_first_level' => $tax_bracket_first_level,
    'tax_payed_up_to_each_bracket_level_1' => $tax_payed_up_to_each_bracket_level_1,
    'remainder_level_1' => $remainder_level_1,
    'marginal_tax_rate_1' => $marginal_tax_rate_1,
    'average_tax_rate_level_1' => $average_tax_rate_level_1,
    'levels' => $levels,
);

if ($return) {
    wp_send_json($return);
}


