import React from 'react'
import PropTypes from 'prop-types'
import ReactDom from 'react-dom'
import * as d3 from 'd3'

import styles from './bar.scss'

export default class BarSummaryChart extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      className: this.props.className,
      width: this.props.width,
      height: this.props.height,
      data: this.props.data,
      duration: this.props.duration,
      customText: this.props.customText
    }
  }

  // The canvas is drawn when the component is mounted.
  componentDidMount () {
    this.draw(this.props.className, this.props.width, this.props.height, this.state.data, this.props.duration, this.props.customText)
  }

  // The canvas is drawn when the component updates.
  componentDidUpdate () {
    d3.selectAll('.' + this.props.className + '> svg').remove()
    this.draw(this.props.className, this.props.width, this.props.height, this.state.data, this.props.duration, this.props.customText)
  }

  draw (className, width, height, data, duration, customText) {
    console.log('draw', data)
    let chartWidth, chartHeight
    let margin
    let svg = d3.select('.' + className).append('svg')
    let chartLayer = svg.append('g').classed('chartLayer', true)
    
    let xScale = d3.scaleBand()
    let yScale = d3.scaleLinear()



    margin = { top:15, left:30, bottom:15, right:0 }

    chartWidth = width - (margin.left + margin.right)
    chartHeight = height - (margin.top + margin.bottom)
    
    svg.attr('width', width).attr('height', height)
    
    chartLayer
        .attr('width', chartWidth)
        .attr('height', chartHeight)
        .attr('transform', 'translate(' + [margin.left, margin.top] + ')')

    xScale.domain(data.map(function (d) { return d.class })).range([0, chartWidth])
        .paddingInner(0.0)
        .paddingOuter(0.0)
        
    yScale.domain([0, d3.max(data, function (d) { return d.value })]).range([chartHeight, 0])

    let t = d3.transition()
      .duration(1000)
      .ease(d3.easeLinear)
      .on('start', function (d) { /* */ })
      .on('end', function (d) { /* */ })

    let bar = chartLayer.selectAll('.' + className).data(data)  
    bar.enter().append('rect').classed(className, true)
        .merge(bar)
        .attr('width', 15)
        .attr('height', 0)
        .attr('class', function (d) { return d.class + ' ' + className })
        .attr('transform', function (d) { return 'translate(' + [xScale(d.class), chartHeight] + ')' })
        .attr('rx', 5)
        .attr('ry', 5)

    // let up = bar.enter().append('text').classed('label label-up', true)
    // let down = bar.enter().append('text').classed('label label-down', true)

    chartLayer.selectAll('.' + className).transition(t)
        .attr('height', function (d) { return chartHeight - yScale(d.value) })
        .attr('transform', function (d) { return 'translate(' + [xScale(d.class), yScale(d.value)] + ')' })
  }

  render () {
    return (
      <div className={this.props.className + ' bar bar-summary'}>
        {/* {bar} */}
      </div>
    )
  }
}

BarSummaryChart.propTypes = {
  className: PropTypes.any,
  width: PropTypes.any,
  height: PropTypes.any,
  data: PropTypes.any,
  duration: PropTypes.any,
  customText: PropTypes.any
}

BarSummaryChart.defaultProps = {
  className: '',
  width: 0,
  height: 0,
  duration: 0,
  customText: ''
}