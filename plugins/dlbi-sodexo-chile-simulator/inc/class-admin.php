<?php

class SodexoChiliSimulator_Admin {

    public function __construct() {
	add_action('init', array($this, 'sodexo_add_options_page'));
    }

    /**
     * Add ACF option page
     * 
     */
    public function sodexo_add_options_page() {

	if (function_exists('acf_add_options_page')) {
	    // add parent for simulator settings
	    $parent = acf_add_options_page(array(
		'page_title' => 'Chili simulator settings',
		'menu_title' => 'Chili simulator settings',
		'redirect' => false
	    ));
	}
    }

}
